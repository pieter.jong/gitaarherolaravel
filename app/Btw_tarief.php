<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Btw_tarief
 *
 * @property int $id
 * @property int $docent_id
 * @property int $percentage
 * @property string $omschrijving
 * @property int|null $default
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Btw_tarief newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Btw_tarief newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Btw_tarief query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Btw_tarief whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Btw_tarief whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Btw_tarief whereDocentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Btw_tarief whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Btw_tarief whereOmschrijving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Btw_tarief wherePercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Btw_tarief whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Btw_tarief extends Model
{
    public $table = "btw_tarieven";
    protected $guarded = [];
}
