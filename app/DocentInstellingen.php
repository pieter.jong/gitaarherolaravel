<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\DocentInstellingen
 *
 * @property int $id
 * @property int $docent_id
 * @property string|null $email
 * @property string|null $naam
 * @property string|null $achternaam
 * @property string|null $bedrijfslogo
 * @property string|null $bedrijfsnaam
 * @property string|null $straat
 * @property string|null $huis_nr
 * @property string|null $postcode
 * @property string|null $woonplaats
 * @property string|null $telefoon
 * @property string|null $kvk_nr
 * @property string|null $btw_nr
 * @property string|null $iban_nr
 * @property string|null $bedrijfs_email
 * @property string|null $bedrijfs_website
 * @property int|null $default_les_minuten
 * @property float|null $default_les_tarief
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|DocentInstellingen newModelQuery()
 * @method static Builder|DocentInstellingen newQuery()
 * @method static Builder|DocentInstellingen query()
 * @method static Builder|DocentInstellingen whereAchternaam($value)
 * @method static Builder|DocentInstellingen whereBedrijfsEmail($value)
 * @method static Builder|DocentInstellingen whereBedrijfsWebsite($value)
 * @method static Builder|DocentInstellingen whereBedrijfslogo($value)
 * @method static Builder|DocentInstellingen whereBedrijfsnaam($value)
 * @method static Builder|DocentInstellingen whereBtwNr($value)
 * @method static Builder|DocentInstellingen whereCreatedAt($value)
 * @method static Builder|DocentInstellingen whereDefaultLesMinuten($value)
 * @method static Builder|DocentInstellingen whereDefaultLesTarief($value)
 * @method static Builder|DocentInstellingen whereDocentId($value)
 * @method static Builder|DocentInstellingen whereEmail($value)
 * @method static Builder|DocentInstellingen whereHuisNr($value)
 * @method static Builder|DocentInstellingen whereIbanNr($value)
 * @method static Builder|DocentInstellingen whereId($value)
 * @method static Builder|DocentInstellingen whereKvkNr($value)
 * @method static Builder|DocentInstellingen whereNaam($value)
 * @method static Builder|DocentInstellingen wherePostcode($value)
 * @method static Builder|DocentInstellingen whereStraat($value)
 * @method static Builder|DocentInstellingen whereTelefoon($value)
 * @method static Builder|DocentInstellingen whereUpdatedAt($value)
 * @method static Builder|DocentInstellingen whereWoonplaats($value)
 * @mixin \Eloquent
 */
class DocentInstellingen extends Model
{
    const OMSCHRIJVING = 'omschrijving';
    const DOCENT_ID = 'docent_id';
    const DEFAULT = 'default';

    public $table = "docent_instellingen";
    protected $guarded = [];

    public function calculatePercentage($db_velden_array){

        $teller = 0;
        foreach ($db_velden_array as $key) {
            if($this->$key > ''){
                $teller ++;
            }
        }
        return round($teller/count($db_velden_array)*100);

    }

    public function create_default_setup($user)
    {
        // btw 0 btw 21 toevoegen en default maken.
        // verkoop product toevoegen. Gitaarles.
        // DocentInstellingen object aanmaken

        DocentInstellingen::create(['' . self::DOCENT_ID . '' =>$user->id]);

        $btw_21 = Btw_tarief::create([self::DOCENT_ID => $user->id, 'percentage' => '21', '' . self::OMSCHRIJVING . '' => 'Btw 21%', '' . self::DEFAULT . '' => '1']);
        $btw_0 = Btw_tarief::create([self::DOCENT_ID => $user->id, 'percentage' => '0', self::OMSCHRIJVING => 'Btw 0%', self::DEFAULT => '2']);
        VerkoopProduct::create(['btw_tarief_id'=>$btw_0->id, self::DOCENT_ID =>$user->id,'tarief'=>'15', self::OMSCHRIJVING =>'Gitaarles jonger dan 21', self::DEFAULT =>'1']);
        VerkoopProduct::create(['btw_tarief_id'=>$btw_21->id, self::DOCENT_ID =>$user->id,'tarief'=>'15', self::OMSCHRIJVING =>'Gitaarles ouder dan 21', self::DEFAULT =>'2']);
    }


}

