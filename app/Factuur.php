<?php

namespace App;

use Faker\Provider\DateTime;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Factuur
 *
 * @property int $id
 * @property int $leerling_id
 * @property int $docent_id
 * @property string $factuur_nr
 * @property string $factuur_datum
 * @property string $verloop_datum
 * @property float $subtotaal
 * @property float $totaal
 * @property int $betaal_termijn
 * @property int $btw_percentage
 * @property string $email_contactpersoon
 * @property string $pdf_bestand
 * @property int $verstuurd
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\FactuurBtw[] $factuur_btws
 * @property-read int|null $factuur_btws_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\FactuurRecord[] $factuur_records
 * @property-read int|null $factuur_records_count
 * @property-read mixed $geboortedatum
 * @property-read \App\Leerling $leerling
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur whereBetaalTermijn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur whereBtwPercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur whereDocentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur whereEmailContactpersoon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur whereFactuurDatum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur whereFactuurNr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur whereLeerlingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur wherePdfBestand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur whereSubtotaal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur whereTotaal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur whereVerloopDatum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Factuur whereVerstuurd($value)
 * @mixin \Eloquent
 */
class Factuur extends Model
{
    public $table = "factuur";
    //

    public function factuur_records(){
        return $this->hasMany(FactuurRecord::class);
    }

    public function factuur_btws(){
        return $this->hasMany(FactuurBtw::class);
    }

    public function leerling(){
        return $this->belongsTo(Leerling::class);
    }

    // Setters

    // Getters
    public function getFactuurDatumAttribute($value)
    {
        if ($value) {
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
    }
    public function getVerloopDatumAttribute($value)
    {
        if ($value) {
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
    }
    public function getGeboortedatumAttribute($value)
    {
        if ($value) {
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
    }
}
