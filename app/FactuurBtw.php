<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class FactuurBtw
 *
 * @package App
 * @mixin Eloquent
 * @property int $id
 * @property int $docent_id
 * @property int $factuur_id
 * @property int $btw_tarieven_id
 * @property string $omschrijving
 * @property float $totaal
 * @property int $percentage
 * @property string $verkoop_datum
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Factuur $factuur
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FactuurBtw newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FactuurBtw newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FactuurBtw query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FactuurBtw whereBtwTarievenId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FactuurBtw whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FactuurBtw whereDocentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FactuurBtw whereFactuurId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FactuurBtw whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FactuurBtw whereOmschrijving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FactuurBtw wherePercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FactuurBtw whereTotaal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FactuurBtw whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FactuurBtw whereVerkoopDatum($value)
 */
class FactuurBtw extends Model
{
    public $table = "factuur_btw";
    protected $guarded = [];

    public function factuur(): BelongsTo
    {
        return $this->belongsTo(Factuur::class);
    }

    // Getters
    public function getVerkoopDatumAttribute($value): ?string
    {
        if ($value) {
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
    }


}
