<?php

namespace App;

use DateTime;
use Eloquent;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\FactuurRecord
 *
 * @property int $id
 * @property int|null $factuur_id
 * @property int $btw_tarief_id
 * @property int $leerling_id
 * @property int $docent_id
 * @property string $omschrijving
 * @property float $qwantiteit
 * @property int $btw_tarief
 * @property float $tarief
 * @property string $verkoop_datum
 * @property int $les_minuten
 * @property string $les_tijdstip
 * @property string $les_datum
 * @property int $verkoop_type
 * @property int|null $default
 * @property string|null $factuur_datum
 * @property int|null $gevalideerd
 * @property int|null $gewijzigd
 * @property int|null $canceled
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Leerling $leerling
 * @method static Builder|FactuurRecord newModelQuery()
 * @method static Builder|FactuurRecord newQuery()
 * @method static Builder|FactuurRecord query()
 * @method static Builder|FactuurRecord whereBtwTarief($value)
 * @method static Builder|FactuurRecord whereBtwTariefId($value)
 * @method static Builder|FactuurRecord whereCanceled($value)
 * @method static Builder|FactuurRecord whereCreatedAt($value)
 * @method static Builder|FactuurRecord whereDefault($value)
 * @method static Builder|FactuurRecord whereDocentId($value)
 * @method static Builder|FactuurRecord whereFactuurDatum($value)
 * @method static Builder|FactuurRecord whereFactuurId($value)
 * @method static Builder|FactuurRecord whereGevalideerd($value)
 * @method static Builder|FactuurRecord whereGewijzigd($value)
 * @method static Builder|FactuurRecord whereId($value)
 * @method static Builder|FactuurRecord whereLeerlingId($value)
 * @method static Builder|FactuurRecord whereLesDatum($value)
 * @method static Builder|FactuurRecord whereLesMinuten($value)
 * @method static Builder|FactuurRecord whereLesTijdstip($value)
 * @method static Builder|FactuurRecord whereOmschrijving($value)
 * @method static Builder|FactuurRecord whereQwantiteit($value)
 * @method static Builder|FactuurRecord whereTarief($value)
 * @method static Builder|FactuurRecord whereUpdatedAt($value)
 * @method static Builder|FactuurRecord whereVerkoopDatum($value)
 * @method static Builder|FactuurRecord whereVerkoopType($value)
 * @mixin Eloquent
 */
class FactuurRecord extends Model
{
    protected $guarded = []; // de tegenhanger hiervan, hier mogen alleen velden in komen die in het formulier voorkomen.
    public $table = "factuur_record";

    // Getters
    public function getLesDatumAttribute($value)
    {
        if ($value) {
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getVerkoopDatumAttribute($value)
    {
        if ($value) {
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
    }

    public function getFactuurDatumAttribute($value)
    {
        if ($value) {
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
    }

    // Setters
    public function setLesDatumAttribute($value)
    {
        if ($value) {
            $this->attributes['les_datum'] = date('Y-m-d', strtotime($value));
        }
    }

    public function leerling()
    {
        return $this->belongsTo(Leerling::class);
    }

    public function salesItem()
    {
        return $this->belongsTo(VerkoopProduct::class,'verkoop_type');
    }

    public function check_obligated($leerling): void
    {
        $btw_hoog = Btw_tarief::where('default', 1)->first();
        if ($btw_hoog) {
            $btw_hoog = $btw_hoog->percentage;
        } else {
            $leerling->error = true;
            $GLOBALS['error']['btw_hoog'] = 'Er is geen "btw hoog tarief" ingevuld bij je "Instellingen".';
        }
        $btw_vrij = Btw_tarief::where('default', 2)->first();
        if ($btw_vrij) {
            $btw_vrij = $btw_vrij->percentage;
        } else {
            $leerling->error = true;
            $GLOBALS['error']['btw_vrij'] = 'Er is geen "btw_vrij tarief" ingevuld bij je "Instellingen".';
        }
        if (!$leerling->geboortedatum) {
            $leerling->error = true;
            $GLOBALS['error'][] = ['geboorte_datum' => '!! Er is geen geboortedatum opgegeven voor ' . $leerling->naam . ' ' . $leerling->achternaam . ' Hierdoor weten we niet welk belasting tarief we moeten hanteren'];
        }
    }


    public function calculate_belasting_tarief($leerling)
    {
        $this->check_obligated($leerling);
        if (!$leerling->error) {
            $btw_hoog = Btw_tarief::where('default', 1)->first();
            $btw_vrij = Btw_tarief::where('default', 2)->first();
            $geboorte_datum = new DateTime($leerling->geboortedatum);
            $les_datum = new DateTime($this->les_datum);
            $diff = $les_datum->diff($geboorte_datum)->format("%a");
            return ($diff < 7665) ? $btw_vrij->percentage : $btw_hoog->percentage / 100;
        }
    }

    /**
     * @param $leerling
     * @return mixed
     * @throws Exception
     */
    public function determan_btw_object($leerling)
    {
        // Is deze controle nodig. Deze controle is al gedaan bij calculate_belasting_tarief
        $btw_hoog = Btw_tarief::where('default', 1)->first();
        if (!$btw_hoog) {
            $leerling->error = true;
        }
        $btw_vrij = Btw_tarief::where('default', 2)->first();
        if (!$btw_vrij) {
            $leerling->error = true;
        }
        if (!$leerling->geboortedatum) {
            $leerling->error = true;
            $GLOBALS['error'][] = ['geboorte_datum' => '!! Er is geen geboortedatum opgegeven voor ' . $leerling->naam . ' Hierdoor weten we niet welk belasting tarief we moeten hanteren'];
        }
        if (!$leerling->error) {
            $btw_hoog = Btw_tarief::where('default', 1)->first();
            $btw_vrij = Btw_tarief::where('default', 2)->first();
            $geboorte_datum = new DateTime($leerling->geboortedatum);
            $les_datum = new DateTime($this->les_datum);
            $diff = $les_datum->diff($geboorte_datum)->format('%a');
            return ($diff < 7665) ? $btw_vrij->id : $btw_hoog->id;
        }
    }

    public function setProperties(Leerling $leerling, string $lesson_date): FactuurRecord
    {
        $this->leerling_id = $leerling->id;
        $this->docent_id = auth()->id();
        $this->qwantiteit = 1;
        $this->tarief = $leerling->les_tarief;
        $this->verkoop_datum = $lesson_date; // Dit is voor niet les records?
        $this->les_minuten = $leerling->les_minuten;
        $this->les_tijdstip = $leerling->les_tijdstip;
        $this->les_datum = $lesson_date; // verkoop_datum is een betere benaming.
        $this->btw_tarief = $this->calculate_belasting_tarief($leerling);
        $this->btw_tarief_id = $this->determan_btw_object($leerling);
        $this->verkoop_type = 1; // wordt verkoop_product_id;
        $this->setSalesItemBasedOnAge($leerling);
        return $this;
    }

    public function validate()
    {
        $attributes = request()->validate([
            'les_datum' => ['required'], // datum validatie
            'les_tijdstip' => ['required'],
            'les_minuten' => ['required'],
            'omschrijving' => ['required'],
            'tarief' => ['required'],
            'qwantiteit' => ['required'],
            'btw_tarief_id' => ['required', 'int'],
        ]);
        // controleren of er ook echt iets is veranderd.
        return $attributes;
    }

    public function additional_values()
    {
        $this['gewijzigd'] = 1;
        $this['btw_tarief'] = 1;
        $this['verkoop_datum'] = date('Y-m-d');
        $this['verkoop_type'] = 1;
        $user = auth()->user();
        if ($user->group == 'docenten') {
            $this['docent_id'] = $user->id;
        }
    }

    /**
     * @param Leerling $leerling
     * @throws Exception
     */
    private function setSalesItemBasedOnAge(Leerling $leerling): void
    {
        $dayOfBirth = new DateTime($leerling->geboortedatum);
        $dateOfLesson = new DateTime($this->les_datum);

        $belowTwentyOne = VerkoopProduct::where('default', VerkoopProduct::STATIC_SALES_ITEM_YOUNGER_THAN_21)->first();
        $aboveTwentyOne = VerkoopProduct::where('default', VerkoopProduct::STATIC_SALES_ITEM_OLDER_THAN_21)->first();


        if ($dayOfBirth->diff($dateOfLesson, true)->days > 7665) {
            $this->verkoop_product_id = $aboveTwentyOne->id;
        } else {
            $this->verkoop_product_id = $belowTwentyOne->id;
        }


    }


}
