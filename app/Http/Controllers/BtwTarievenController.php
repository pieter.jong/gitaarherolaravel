<?php

namespace App\Http\Controllers;

use App\Btw_tarief;
use Illuminate\Http\Request;

class BtwTarievenController extends Controller
{
    public function index()
    {
        $btw_tarieven = Btw_tarief::where('docent_id', auth()->id())->get();
        return view('leerlingen.index', compact('btw_tarieven'));
    }


    public function create()
    {
        return view('btwtarieven.create');
    }


    public function store()
    {
        $attributes = request()->validate([
            'percentage'=>['required', 'int'],
            'omschrijving'=>'required',
        ]);
        $attributes['docent_id'] = auth()->id();
        Btw_tarief::create($attributes);
        return redirect('/instellingen');
    }

//    public function show($id)
//    {
//        $btw_tarieven = Btw_tarief::find($id);
//        return view('btwtarieven.show',compact('btw_tarieven'));
//    }

    public function edit($id)
    {
        $btw_tarief = Btw_tarief::where('docent_id', auth()->id())->where('id', $id)->first();
        if($btw_tarief){
            return view('btwtarieven.edit', compact('btw_tarief'));
        }else{
            // error "Het Btw tarief wat je probeert te bewerken bestaat niet."
            return back();
        }
    }

    public function update($id)
    {
        $attributes = request()->validate([
            'percentage'=>['required', 'int'],
            'omschrijving'=>'required',
        ]);
        $btw_tarief = Btw_tarief::where('docent_id', auth()->id())->where('id', $id)->first();
        request()['docent_id'] = auth()->id();
        if($btw_tarief)
            $btw_tarief->update(request()->all());
        return redirect('/instellingen');
    }


    public function destroy($id)
    {
        $btw_tarief = Btw_tarief::where('docent_id', auth()->id())->where('id', $id)->first();
        if($btw_tarief)
            $btw_tarief->delete();
        return redirect('/instellingen');
    }
}
