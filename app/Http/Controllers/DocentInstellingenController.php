<?php

namespace App\Http\Controllers;

use App\DocentInstellingen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image; // we kunne hier ook een andere allias voor opgeven in config/app mocht dit problemen opleveren
use Response;
// use FontLib\TrueType\File;
use File;

class DocentInstellingenController extends Controller
{



 public function docent_gegevens_edit(){
        $docent_instellingen = DocentInstellingen::where('docent_id', auth()->id())->first();
        return view('docent.mijn_gegevens',compact('docent_instellingen'));
    }

    public function docent_gegevens_update(){
        //openstaand:
        // controle op email, foto_upload
        // Sla gegevens op onder bestandsnaam van docent.
        $bedrijfslogo = Input::file('bedrijfslogo');
        $attributes = request()->all();
        if($bedrijfslogo){
            $filename = $bedrijfslogo->getClientOriginalName();
            $path = public_path().'/bedrijfslogos/';
            File::exists($path) or File::makeDirectory($path);
            $image = Image::make($bedrijfslogo->getRealPath());
            $image->resize(400, null, function ($constraint) { // with, height, callback
                $constraint->aspectRatio();
            });
            $image->save($path.$filename);
            $attributes['bedrijfslogo'] = '/bedrijfslogos/'.$filename;
        }
        
        $docent_instellingen = DocentInstellingen::where('docent_id', auth()->id())->first();
        $docent_instellingen->update($attributes);
        return redirect('/instellingen');
    }


    public function leswaardes_edit(){
        $docent_instellingen = DocentInstellingen::where('docent_id', auth()->id())->first();
        return view('docent.leswaardes',compact('docent_instellingen'));
    }

    public function leswaardes_update(){
        $docent_instellingen = DocentInstellingen::where('docent_id',auth()->id())->first();
        $docent_instellingen->update(request()->all());
        return redirect('/instellingen');
    }
}
