<?php

namespace App\Http\Controllers;

use App\Leerling;
use App\Repositories\LeerlingRepository;
use App\Vakantie;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class DocentVakantieController extends Controller
{
    /**
     * @var LeerlingRepository
     */
    private $leerlingRepository;

    public function __construct(LeerlingRepository $leerlingRepository)
    {
        $this->leerlingRepository = $leerlingRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function create(int $id)
    {
        return view('vakanties.docentVakantieCreate',compact('id'));
    }

    /**
     * @param $id
     * @return RedirectResponse|Redirector
     */
    public function store($id)
    {
        // Hier zou je een leerling $id kunnen injecten.
        $attributes = request()->validate([
            'start_datum'=>['required'],
            'eind_datum'=>[''],
            'omschrijving'=>[''],
        ]);
        $attributes['vakantie_type'] = Vakantie::HOLIDAY_TYPE_TEACHER;
        // $attributes['leerling_id'] = $id; // dit kunnen we ook in het migration ding regellen
        $attributes['docent_id'] = auth()->id(); // dit kunnen we ook in het migration ding regellen
        $vakantie = Vakantie::create($attributes);

        $leerling = Leerling::where('docent_id', auth()->id())->where('id', $id)->first();
        $this->leerlingRepository->cancelAndRestoreLessons($leerling, $vakantie, true);
        return redirect()->route('leerling.show', $leerling);
    }


    public function edit($leerling_id,$vakantie_id)
    {
        $vakantie = Vakantie::where('docent_id',auth()->id())->where('id',$vakantie_id)->first();
        $leerling = Leerling::where('docent_id',auth()->id())->where('id',$leerling_id)->first();
        return view('vakanties.docentVakantiesEdit',compact('vakantie', 'leerling'));
    }

    public function update($leerling_id, $vakantie_id)
    {
        // Hier zou je een docent_id kunnen injecten.
        $attributes = request()->validate([
            'start_datum' => ['required'],
            'eind_datum' => [''],
            'omschrijving' => [''],
        ]);
        $vakantie = Vakantie::where('docent_id', auth()->id())->where('id', $vakantie_id)->first();
        $leerling = Leerling::where('docent_id', auth()->id())->where('id', $leerling_id)->first();
        if($vakantie)
            $vakantie->update(request()->all());
        $this->leerlingRepository->cancelAndRestoreLessons($leerling, $vakantie, true);
        return redirect()->route('leerling.show', $leerling);
    }

    public function destroy($leerling_id, $vakantie_id)
    {
        $vakantie = Vakantie::where('docent_id', auth()->id())->where('id', $vakantie_id)->first();
        $leerling = Leerling::where('docent_id', auth()->id())->where('id', $leerling_id)->first();
        if($vakantie)
            $vakantie->delete();
        $this->leerlingRepository->cancelAndRestoreLessons($leerling, $vakantie, false);
        return redirect()->route('leerling.show', $leerling);
    }

}
