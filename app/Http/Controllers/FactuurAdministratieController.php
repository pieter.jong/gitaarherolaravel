<?php

namespace App\Http\Controllers;

use App\DocentInstellingen;
use App\Factuur;
use App\FactuurRecord;
use App\Leerling;
use App\Repositories\FactuurRepository;
use App\Repositories\LeerlingRepository;
use Dompdf\Dompdf;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Validator;


class FactuurAdministratieController extends Controller
{

    /**
     * @var LeerlingRepository
     */
    private $leerlingRepository;
    /**
     * @var FactuurRepository
     */
    private $factuurRepository;

    public function __construct(LeerlingRepository $leerlingRepository, FactuurRepository $factuurRepository)
    {
        $this->leerlingRepository = $leerlingRepository;
        $this->factuurRepository = $factuurRepository;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $invoices = Factuur::where('docent_id', auth()->id())->get();
        $products = FactuurRecord::where('docent_id', auth()->id())->get();
        return view('factuur_administratie.index')->with('facturen', $invoices);
    }

    /**
     * @return Application|RedirectResponse|Redirector
     */
    public function generateLessons()
    {
        $lessonRecords = null;
        $messages = ['date_format' => ':attribute voldoet niet aan het datum formaat dd-mm-yyyy'];
        $validation_fields = [
            'start_datum' => 'required|date_format:d-m-Y',
            'eind_datum' => 'required|date_format:d-m-Y',
        ];
        // $custom_error_class = new custom_error_class();
        // $validator = $custom_error_class->controller_error_handler($validation_fields, $messages);
        $validator = Validator::make(request()->all(), $validation_fields, $messages);


        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $startDate = date('Y-m-d', strtotime(request('start_datum')));
        $endDate = date('Y-m-d', strtotime(request('eind_datum')));
        if (!array_key_exists('error', $GLOBALS)) {
            $pupils = Leerling::where('docent_id', auth()->id())->get();

            /** @var Leerling $pupil */
            foreach ($pupils as $pupil) {
                $this->leerlingRepository->planLessons($pupil, $startDate, $endDate);
            }
        }

        $custom_error_class = new custom_error_class();
        $custom_error_class->ControllerErrorHandler($validator); // is het nogsteeds nuttig om de custom class hiervoor te gebruiken?
        return redirect('factuur_administratie/?ok=1');
    }


    /**
     * @return RedirectResponse
     */
    public function generateInvoice(): RedirectResponse
    {
        $validator = Validator(request()->all());
        // Als er meer dan 10 lessen op een factuur komen te staan, dan niet versturen en Docent inlichten.

        // 1 Vast bedrag per maand
        // 2 1 factuur per halfjaar
        // 3 1 factuur per jaar
        // 4 Maandelijks vooruit genereren
        // 5 Maandelijks achteraf aanmaken
        // 6 Handmatig.

        $start_datum = '';
        $eind_datum = '';

        $option = 4;

        if ($option === 1) {
            // factuur is niet op basis van de lessen maar een vast bedrag.
            // Factuur omschrijving -> "wekelijks gitaarles voor een vast bedrag per maand"
        }
        if ($option === 2) {
            // factuur wordt verstuurd met eenmalig bedrag.
            // einddatum moet worden bijgehouden
            // Factuur omschrijving -> "wekelijks gitaarles voor een vast bedrag per halfjaar"
        }
        if ($option === 3) {
            // factuur wordt verstuurd met eenmalig bedrag.
            // einddatum moet worden bijgehouden
            // Factuur omschrijving -> "wekelijks gitaarles voor een vast bedrag per jaar"
            // Alle lessen worden gekoppeld aan 1 jaar_factuur.
        }
        if ($option === 4) {
            // lessen worden bevestigd op het moment van versturen factuur, leerlingen kunnen alleen vooraf afzeggen.
            $pupils = Leerling::where('docent_id', auth()->id())->get();
            foreach ($pupils as $pupil) {
                $this->controllerErrorHandler($validator);
                $this->factuurRepository->createInvoice($pupil);
                // confirm_lessons_for_invoice
            }


            // pdf bestanden maken.-------------------
            $invoices = Factuur::where('docent_id', auth()->id())->get();
            foreach ($invoices as $invoice) {
                $this->makePdf($invoice);
            }

        }
        if ($option === 5) {
            // Lessen zijn automatisch bevestigd op het moment dat de datum is verstreken.
        }
        if ($option === 6) {
            // lessen worden bevestigd tot de opgegeven eind_datum op het moment van versturen factuur, leerlingen kunnen alleen vooraf afzeggen.
        }

        return back();
    }


    public function scheduledInvoice($id)
    {
        $pupil = Leerling::where('docent_id', auth()->id())->where('id', $id)->first();
        $invoiceRecords = $this->leerlingRepository->recordsBasedOnInvoiceSchedule($pupil);
        $settings = DocentInstellingen::where('docent_id', auth()->id())->first();
        if (!empty($settings)) {
            $logo = $settings->bedrijfslogo;
        }
        $invoice = new Factuur();

        $invoice->factuur_nr = $this->factuurRepository->createInvoiceNumber();
        $invoice->factuur_datum = date('d-m-Y');
        $btw = $this->factuurRepository->createInvoiceTax($pupil, $invoiceRecords);
        $subtotal = $btw['subtotaal'];
        $total = $btw['totaal'];
        $taxRates = $btw['btw_tarieven'];
        $invoiceRecordEditRoute = '';
        $holidayCreateCancelLesson = '';


        return view('factuur_administratie.scheduled_invoice')
            ->with('factuur', $invoice)
            ->with('totaal', $total)
            ->with('subtotaal', $subtotal)
            ->with('btw_tarieven', $taxRates)
            ->with('leerling', $pupil)
            ->with('logo', $logo)
            ->with('instellingen', $settings)
            ->with('factuurRecordEditRoute', $invoiceRecordEditRoute)
            ->with('vakantieCreateCancelLesson', $holidayCreateCancelLesson)
            ->with('invoiceRecords', $invoiceRecords);
    }

    private function makePdf(Factuur $invoice): void
    {
        $pupil = $invoice->leerling()->first();
        $tax = $invoice->factuur_btws()->get();

        $settings = DocentInstellingen::where('docent_id', auth()->id())->first();
        $invoiceRecords = $invoice->factuur_records()->with('salesItem')->get();
        $logo = public_path() . $settings->bedrijfslogo;
        if (count($invoiceRecords)){
//            dd($invoiceRecords);
        }

        $html = view('factuur_administratie.factuur_layout')
            ->with('factuur', $invoice)
            ->with('invoiceRecords', $invoiceRecords)
            ->with('btw', $tax)
            ->with('leerling', $pupil)
            ->with('logo', $logo)
            ->with('invoiceMonths', $this->factuurRepository->showInvoiceMonths($invoice))
            ->with('instellingen', $settings);

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4');
        // $dompdf->set_options()

        // Render the HTML as PDF
        $dompdf->render();
        $output = $dompdf->output();
        // unset($dompdf);
        // $file = new Input();
        // $file->move

        file_put_contents(sprintf(
            '%s/%s/%s.pdf', $_SERVER['DOCUMENT_ROOT'], env('PDF_STORAGE'), $invoice->factuur_nr),
            $output
        );
    }

    private function controllerErrorHandler($validator)
    {
        if (array_key_exists('error', $GLOBALS)) {
            $validator->after(function ($validator) {
                $test = new custom_error_class();
                $errors = $test->ErrorMessageToList($GLOBALS['error']);
                foreach ($errors as $key => $message) {
                    $validator->errors()->add($key, $message);
                }
            });

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }
        }
    }
}
