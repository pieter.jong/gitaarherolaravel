<?php

namespace App\Http\Controllers;

use App\FactuurRecord;
use App\Leerling;
use App\VerkoopProduct;
use Illuminate\Http\Request;

class FactuurRecordController extends Controller
{

    public function index()
    {
        // kan verschillen per omgeving.
    }

    public function create()
    {
        // laat formulier zien
        // return view('factuur_records.create');
    }

    public function store()
    {
    }

    /**
     * @param int $id
     * @return array|null
     */
    public function editTest(int $id): ?array
    {
        //Todo betere naam bedenken voor deze functie
        //Het id dat wordt mee gegeven aan de url is niet juist
        $lock = false;
        $factuur_record = FactuurRecord::where('docent_id', auth()->id())->where('id', $id)->first();
        if (!$factuur_record) {
            return null; //deze zorgt niet voor een return back, die is alleen vanuit de initiele functie aan te roepen.. Nu kunnen we niet op een compact returnType vertrouwen
        }
        $leerling_id = $factuur_record->leerling()->first()->id;

        if ($factuur_record->verkoop_type === 1) {
            $lock = true;
        }
        return compact('lock', 'factuur_record', 'leerling_id');
    }

    public function update($id)
    {
    }

    public function destroy($id)
    {
        // is algemeen, omdat deze altijd returned naar back(). Wordt wel anders bij
        $factuur_record = FactuurRecord::where('docent_id', auth()->id())->where('id', $id)->first();
        if ($factuur_record) {
            $factuur_record->delete();
        }
        return back();
    }

    public function create_for_leerling($id)
    {
        $verkoop_producten = VerkoopProduct::where('docent_id', auth()->id())->get();
        $leerling = Leerling::where('docent_id', auth()->id())->where('id', $id)->first();
        $store_route = '';
        $back_route = '';
        if ($leerling) {
            return view('factuur_records.create', compact('leerling', 'verkoop_producten'));
        } else {
            return back(); // error mee geven "geen rechten tot deze leerling."
        }

    }
    public function store_for_leerling($id)
    {
        $leerling = Leerling::where('docent_id', auth()->id())->where('id', $id)->first();
        if ($leerling) {
            $factuur_record = new FactuurRecord();
            $attributes = $factuur_record->validate();
            $factuur_record = FactuurRecord::make($attributes);
            $factuur_record->docent_id = auth()->id();
            $factuur_record->btw_tarief = 1;
            $factuur_record->verkoop_datum = '1982-11-30';
            $factuur_record->verkoop_type = 1;
            $leerling->invoiceRecords()->save($factuur_record);
        } else {
            return back(); // Melding geven deze leerling bestaat niet.
        }

        return redirect('/leerling/' . $leerling->id);
    }
    public function edit_for_leerling($id)
    {
        if(!$compact = $this->editTest($id)){
            return back();
        }
//        Todo doet laravel ook iets met route names? voobeeld(route('leerlingen.show',['leerlingen'=>$compact['leerling_id']]);

        $back_route = '/leerlingen/' . $compact['leerling_id'];
        $update_route = '/leerling/factuur_record/update/' . $compact['factuur_record']->id;
        $compact = array_merge($compact, compact('back_route', 'update_route'));
        return view('factuur_records.edit', $compact);
    }
    public function update_for_leerling($id){
        $factuur_record = FactuurRecord::where('docent_id', auth()->id())->where('id', $id)->first();
        if (!$factuur_record) {
            return back(); // met foutmelding
        }
        $attributes = $factuur_record->validate();
        $factuur_record->additional_values();
        $factuur_record->update($attributes);
        $leerling = Leerling::where('id', $factuur_record->leerling_id)->first();
        $leerling->cancelAndRestoreLessons(null,1);
        return redirect('/leerlingen/'.$leerling->id);
    }

    public function destroy_for_leerling($id){
        // andere return
    }


    public function update_for_leerlingoverzicht(){

    }
    // Het lijkt gek dat er een edit functie op deze controller zit. Dit wordt toch altijd vanuit andere controllers geregeld.
    // 2 controller types.
    // Voor objecten die niet echt een overzicht pagina hebben (factuur_record)
    // Voor objecten die wel een overzicht pagina hebben. (leerlingOverzicht)

    public function edit_for_leerlingoverzicht(){
        $compact = $this->edit($id);
        $factuur_record_back_route = '/leerling_overzicht/';
        $factuur_record_edit_route = '/leerling_overzicht/factuur_record/update/' . $compact['factuur_record']->id;
        $compact = array_merge($compact, compact('factuur_record_back_route', 'factuur_record_update_route'));
        return view('factuur_records.edit', $compact);
    }
}
