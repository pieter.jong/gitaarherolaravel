<?php

namespace App\Http\Controllers;

use App\Btw_tarief;
use App\DocentInstellingen;
use Illuminate\Http\Request;

class InstellingenController extends Controller
{

    public function index()
    {
        $mijn_gegeven_array = [
            'naam',
            'achternaam',
            'email',
            'straat',
            'huis_nr',
            'postcode',
            'woonplaats',
            'telefoon',
            'bedrijfsnaam',
            'kvk_nr',
            'btw_nr',
            'iban_nr',
            'bedrijfslogo'
        ];
        $lesgegevens_array = ['default_les_minuten', 'default_les_tarief'];
        $taxRates = Btw_tarief::where('docent_id', auth()->id())->get();
        $teacherSettings = DocentInstellingen::where('docent_id', auth()->id())->first();
        $mijn_gegeven_percentage = $teacherSettings->calculatePercentage($mijn_gegeven_array);
        $lesgegevens_percentage = $teacherSettings->calculatePercentage($lesgegevens_array);
        return view('instellingen.index')
            ->with('btw_tarieven', $taxRates)
            ->with('mijn_gegeven_percentage', $mijn_gegeven_percentage)
            ->with('lesgegevens_percentage', $lesgegevens_percentage);
    }
}
