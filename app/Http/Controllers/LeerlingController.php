<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLeerling;
use App\Leerling;
use App\Repositories\LeerlingRepository;
use App\Vakantie;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

/**
 * Class LeerlingController
 * @mixin Eloquent
 * @package App\Http\Controllers
 */
class LeerlingController extends Controller
{
    /**
     * @var LeerlingRepository
     */
    private $leerlingRepository;

    public function __construct(LeerlingRepository $leerlingRepository)
    {
        $this->middleware('auth');
        $this->leerlingRepository = $leerlingRepository;
    }

    /**
     * @return Factory|Application|View
     */
    public function create()
    {
        return view('leerlingen.create');
    }

    /**
     * @param StoreLeerling $request
     * @return RedirectResponse|Redirector
     */
    public function store(StoreLeerling $request)
    {
        $this->leerlingRepository->store($request);

        return redirect()->route('leerlingoverzicht');
    }

    /**
     * @param Leerling $leerling
     * @return Factory|Application|RedirectResponse|View
     */
    public function show(Leerling $leerling)
    {
        if ($leerling->docent_id !== auth()->id()) {
            return back();
        }

        $this->leerlingRepository->checkObligated($leerling);

        $holidays = $leerling->vakanties()->get();

        $teacherAbsence = Vakantie::where('docent_id', auth()->id())
            ->where('vakantie_type', Vakantie::HOLIDAY_TYPE_TEACHER)
            ->get();

        $invoiceRecords = $leerling->invoiceRecords()
            ->with('salesItem')
            ->orderBy('les_datum', 'asc')
            ->orderBy('verkoop_datum', 'asc')
            ->get();


        $vakantieCreateCancelLesson = '/leerlingen/cancel_lesson/';
        $factuurRecordEditRoute = '/leerling/factuur_record/edit/';

        return view('leerlingen.show')
            ->with('pupil', $leerling)
            ->with('holidays', $holidays)
            ->with('teacherAbsence', $teacherAbsence)
            ->with('invoiceRecords', $invoiceRecords)
            ->with('factuurRecordEditRoute', $factuurRecordEditRoute)
            ->with('vakantieCreateCancelLesson', $vakantieCreateCancelLesson);
    }

    /**
     * @param Leerling $leerling
     * @return Factory|Application|RedirectResponse|View
     */
    public function edit(Leerling $leerling)
    {
        if ($leerling->docent_id !== auth()->id()) {
            return back();
        }

        $disableFirstLesson = count($leerling->facturen()->get());

        return view('leerlingen.edit')
            ->with('pupil', $leerling)
            ->with('disableFirstLesson', $disableFirstLesson);
    }

    /**
     * @param StoreLeerling $request
     * @param Leerling $leerling
     * @return RedirectResponse
     */
    public function update(StoreLeerling $request, Leerling $leerling): RedirectResponse
    {
        if ($leerling->docent_id !== auth()->id()) {
            return back();
        }

        $this->leerlingRepository->update($request, $leerling);

        return redirect()->route('leerling.show', [$leerling]);
    }

    /**
     * @param Leerling $leerling
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy(Leerling $leerling)
    {
        if ($leerling->docent_id !== auth()->id()) {
            return back();
        }
        $leerling->invoiceRecords()->delete();
        if ($leerling) {
            $leerling->delete();
        }
        return redirect('/leerling_overzicht');
    }
}
