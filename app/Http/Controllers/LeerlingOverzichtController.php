<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Leerling;
use App\FactuurRecord;
use App\Vakantie;
use Validator;
use App\Http\Controllerscustom_error_class;

class LeerlingOverzichtController extends Controller
{

    private const Y_M_D = 'Y-m-d';
    private const LESSONS_THIS_WEEK = 'lessons_this_week';
    private const WEEK_DIFFERENCE = 'week_difference';
    private const FACTUUR_RECORD_EDIT_ROUTE = 'factuur_record_edit_route';
    private const VAKANTIE_CREATE_CANCEL_LESSON_ROUTE = 'vakantie_create_cancel_lesson_route';

    public function index()
    {
        $leerlingen = Leerling::where('docent_id', auth()->id())->get();
        $lessons_this_week = $this->lessons_this_week();
        $week_difference = 0;
        // Hmm... Dit voelt nog niet helemaal logisch.
        $vakantie_create_cancel_lesson_route = '/leerling_overzicht/vakantie/create_cancel_lesson/';
        $factuur_record_edit_route = '/leerling_overzicht/factuur_record/edit/';
        return view('leerling_overzicht.index')
            ->with('leerlingen', $leerlingen)
            ->with(self::LESSONS_THIS_WEEK, $lessons_this_week)
            ->with(self::WEEK_DIFFERENCE, $week_difference)
            ->with(self::FACTUUR_RECORD_EDIT_ROUTE, $factuur_record_edit_route)
            ->with(self::VAKANTIE_CREATE_CANCEL_LESSON_ROUTE, $vakantie_create_cancel_lesson_route);
    }

    public function ajax_lessons_next_week($week_difference)
    {
        $week_difference++;
        $lessons_this_week = $this->lessons_this_week($week_difference);
        $vakantie_create_cancel_lesson_route = '/factuur_record/edit_for_leerlingoverzicht';
        $factuur_record_edit_route = '/factuur_record/edit_for_leerlingoverzicht/' . 'factuur_record_id';
        return view('leerling_overzicht.ajax_lessons_this_week')
            ->with(self::LESSONS_THIS_WEEK, $lessons_this_week)
            ->with(self::WEEK_DIFFERENCE, $week_difference)
            ->with(self::FACTUUR_RECORD_EDIT_ROUTE, $factuur_record_edit_route)
            ->with(self::VAKANTIE_CREATE_CANCEL_LESSON_ROUTE, $vakantie_create_cancel_lesson_route);
    }

    public function ajax_lessons_prev_week($week_difference)
    {
        $week_difference--;
        $lessons_this_week = $this->lessons_this_week($week_difference);
        $vakantie_create_cancel_lesson_route = '/factuur_record/edit_for_leerlingoverzicht';
        $factuur_record_edit_route = '/leerling_overzicht/factuur_record/update/' . 'factuur_record_id';
        return view('leerling_overzicht.ajax_lessons_this_week')
            ->with(self::LESSONS_THIS_WEEK, $lessons_this_week)
            ->with(self::WEEK_DIFFERENCE, $week_difference)
            ->with(self::FACTUUR_RECORD_EDIT_ROUTE, $factuur_record_edit_route)
            ->with(self::VAKANTIE_CREATE_CANCEL_LESSON_ROUTE, $vakantie_create_cancel_lesson_route);
    }

    public function lessons_this_week($week_difference = null)
    {
        $verschil_dagen = 1 - date('w');
        if (!$week_difference) {
            $week_start = date('' . self::Y_M_D . '', strtotime($verschil_dagen . ' days'));
        } else {
            $week_start = date(self::Y_M_D, strtotime($verschil_dagen . ' days + ' . $week_difference . ' week'));
        }
        $week_end = date(self::Y_M_D, strtotime($week_start . ' 6 days'));

        $lessons = FactuurRecord::where('docent_id', auth()->id())
            ->where('les_datum', '>=', $week_start)
            ->where('les_datum', '<=', $week_end)
            ->where('verkoop_type', '1')
            ->get();
        $factuur_record_edit_route = '';
        $vakantie_create_cancel_lesson = '';
        return $lessons;
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
