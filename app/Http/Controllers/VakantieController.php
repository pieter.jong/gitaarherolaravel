<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreVakantie;
use App\Repositories\LeerlingRepository;
use App\Vakantie;
use App\Leerling;
use Illuminate\Http\Request;
use Validator;
use App\FactuurRecord;


class VakantieController extends Controller
{

    /**
     * @var LeerlingRepository
     */
    private $leerlingRepository;

    public function __construct(LeerlingRepository $leerlingRepository)
    {
        $this->leerlingRepository = $leerlingRepository;
    }

    public function index()
    {
        //
    }

    public function create($id)
    {
        $leerling = Leerling::where('docent_id', auth()->id())->where('id', $id)->first();
        if($leerling){
            return view('vakanties.leerlingVakantiesCreate', compact('leerling'));
        }else{
            return back(); // foutmelding geven "leerling bestaat niet."
        }

    }

    public function create_cancel_lesson($id)
    {
        $factuur_record = FactuurRecord::where('docent_id', auth()->id())->where('id',$id)->first();
        if (!$factuur_record) {
            return back(); // foutmelding geven "geen rechten op dit record"
        }
        $leerling = Leerling::where('id', $factuur_record->leerling_id)->first();
        $vakantie_back_route =  '/leerlingen/'.$leerling->id;
        $vakantie_create_cancel_lesson_route = '/leerlingen/'.$leerling->id.'/leerlingvakantie';
        return view('vakanties.leerling_cancel_lesson', compact('leerling','factuur_record', 'vakantie_create_cancel_lesson_route', 'vakantie_back_route'));
    }

    public function create_cancel_lesson_for_leerling_overzicht($id){
        $factuur_record = FactuurRecord::where('docent_id', auth()->id())->where('id', $id)->first();
        if (!$factuur_record) {
            return back(); // foutmelding geven "geen rechten op dit record"
        }
        $leerling = Leerling::where('id', $factuur_record->leerling_id)->first();
        $vakantie_create_cancel_lesson_route = '';
        $vakantie_back_route = '/leerling_overzicht';
        return view('vakanties.leerling_cancel_lesson', compact('leerling', 'factuur_record', 'vakantie_create_cancel_lesson_route', 'vakantie_back_route'));
    }

    public function store(StoreVakantie $request, $id)
    {
        $vakantie = new Vakantie();

        $leerling = Leerling::where('docent_id', auth()->id())->where('id', $id)->first();
        $attributes = $request->validated();

        $attributes['vakantie_type'] = Vakantie::HOLIDAY_TYPE_PUPIL;
        $leerling->addVakantie($attributes);

        $this->leerlingRepository->cancelAndRestoreLessons($leerling, $vakantie, true);

        return redirect()->route('leerling.show', $leerling);
    }

    public function store_for_leerling(){}

    public function store_for_leerling_overzicht(){}

    public function show(vakantie $vakantie)
    {
        //
    }

    public function edit($id)
    {
        $vakantie = vakantie::where('docent_id',auth()->id())->where('id',$id)->first();
        $leerling = Leerling::where('docent_id',auth()->id())->where('id',$vakantie->leerling_id)->first();
        // leerling wordt alleen gebruikt voor het ID volgens mij.
        return view('vakanties.leerlingVakantiesEdit',compact('vakantie', 'leerling'));
    }

    public function messages()
    {
        return [
            'eind_datum.date.after' => 'A message is required',
        ];
    }

    public function update(StoreVakantie $request, $id)
    {
        $vakantie = vakantie::where('docent_id', auth()->id())->where('id', $id)->first();
        if (!$vakantie){
            return back();
        }
        $vakantie->update($request->all());
        $leerling = Leerling::where('docent_id', auth()->id())->where('id', $vakantie->leerling_id)->first();
        $this->leerlingRepository->cancelAndRestoreLessons($leerling, $vakantie, true);

        return redirect()->route('leerling.show', $leerling);
    }

    public function destroy($id)
    {
        $vakantie = vakantie::where('docent_id',auth()->id())->where('id',$id)->first();
        if (!$vakantie){
            return back(); //foutmelding geven
        }
        $leerling = Leerling::where('docent_id', auth()->id())->where('id', $vakantie->leerling_id)->first();
        $vakantie->delete();
        $leerling->cancelAndRestoreLessons($vakantie);
        return redirect('/leerlingen/'.$vakantie->leerling_id);
    }
}
