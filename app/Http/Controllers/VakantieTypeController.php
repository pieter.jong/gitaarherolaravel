<?php

namespace App\Http\Controllers;

use App\vakantie_type;
use Illuminate\Http\Request;

class VakantieTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\vakantie_type  $vakantie_type
     * @return \Illuminate\Http\Response
     */
    public function show(vakantie_type $vakantie_type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\vakantie_type  $vakantie_type
     * @return \Illuminate\Http\Response
     */
    public function edit(vakantie_type $vakantie_type)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\vakantie_type  $vakantie_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, vakantie_type $vakantie_type)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\vakantie_type  $vakantie_type
     * @return \Illuminate\Http\Response
     */
    public function destroy(vakantie_type $vakantie_type)
    {
        //
    }
}
