<?php

namespace App\Http\Controllers;

use App\VerkoopProduct;
use Illuminate\Http\Request;
use App\Btw_tarief;

class VerkoopProductController extends Controller
{
    public function index()
    {
        // genereerd een lijst met verkoop producten
        $docent = '';
        // $verkoop_producten = $docent->verkoop_producten()->get();
        $verkoop_producten = VerkoopProduct::where('docent_id',auth()->id())->get();
        return view('verkoop_producten.index',compact('verkoop_producten'));
    }




    public function create()
    {
        $btw_tarieven = Btw_tarief::where('docent_id',auth()->id())->get();
        return view('verkoop_producten.create',compact('btw_tarieven'));
    }


    public function store()
    {
        // validatie
        // verkoop product wordt aan een docent toegevoegd.
        // return naar instellingen

        $attributes = request()->validate([
            'omschrijving' => ['required', 'min:3'],
            'tarief' => ['required','numeric'],
            'btw_tarief_id' => ['required','int'],
        ]);
        $attributes['docent_id'] = auth()->id();
        VerkoopProduct::create($attributes);
        return redirect('/instellingen/verkoop_product');
    }

    public function show()
    {
        // let op dat hier alleen producten van de huidige docent getoont kunnen worden.
    }

    public function edit($id)
    {
        $verkoop_product = VerkoopProduct::where('docent_id', auth()->id())->first();
        $btw_tarieven = Btw_tarief::where('docent_id', auth()->id())->get();
        // we hebben het leerling->id ook nodig voor de terug knop.
        return view('verkoop_producten.edit',compact('verkoop_product', 'btw_tarieven'));
    }

    public function update($id)
    {
        // return naar instellingen
        $attributes = request()->validate([
            'omschrijving' => ['required', 'min:3'],
            'tarief' => ['required', 'numeric'],
            'btw_tarief_id' => ['required', 'int'],
        ]);
        $verkoop_product = VerkoopProduct::where('docent_id', auth()->id())->first();
        $verkoop_product->update($attributes);
        return redirect('/instellingen/verkoop_product');
    }

    public function destroy($id)
    {
        $verkoop_product = VerkoopProduct::where('docent_id', auth()->id())->first();
        if($verkoop_product){
            if(!$verkoop_product->default){
                $verkoop_product->delete();
            }
        }
        return redirect('/instellingen/verkoop_product');
    }
}
