<?php

namespace App\Http\Controllers;

use Validator;

class custom_error_class extends Controller
{

    public function ErrorMessageToList($array): array
    {
        $errorArray = array();
        foreach ($array as $key => $val) {
            if (is_array($val)) {
                $counter = 1;
                foreach ($val as $anotherVal) {
                    $errorArray[$key . '_' . $counter] = '!! ' . $anotherVal;
                    $counter++;
                }
            } else {
                $errorArray[$key] = '!! ' . $val;
            }
        }
        return $errorArray;
    }


    public function ControllerErrorHandler($validator)
    {
        if (array_key_exists('error', $GLOBALS)) {
            $validator->after(function ($validator) {
                $errors = $this->ErrorMessageToList($GLOBALS['error']);
                foreach ($errors as $key => $message) {
                    $validator->errors()->add($key, $message);
                }
            });

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }
        }
    }


}
