<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLeerling extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'docent_id' => 'required',
            'naam' => 'required|min:3',
            'achternaam' => 'required',
            'email' => 'nullable|email',
            'geboortedatum' => 'nullable',
            'eerste_les' => 'nullable',
            'geadreseerde' => 'nullable',
            'straatnaam' => 'nullable',
            'huisnummer' => 'nullable',
            'postcode' => 'nullable',
            'woonplaats' => 'nullable',
            'telefoon' => 'nullable',
            'les_interval' => 'nullable',
            'les_type' => 'nullable',
            'les_dag' => 'nullable',
            'les_tijdstip' => 'nullable',
            'les_minuten' => 'nullable',
            'les_tarief' => 'nullable',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'docent_id' => auth()->id()
        ]);
    }
}
