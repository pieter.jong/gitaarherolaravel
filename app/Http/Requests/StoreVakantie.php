<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreVakantie extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'docent_id' => 'required',
            'start_datum' => 'required|date_format:d-m-Y',
            'eind_datum' => 'date|afterOrEqual:start_datum',
            'omschrijving' => 'required'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'docent_id' => auth()->id()
        ]);
    }
}
