<?php

namespace App;

use DateTime;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Leerling
 *
 * @property int $id
 * @property int $docent_id
 * @property string $naam
 * @property string $achternaam
 * @property string|null $geadreseerde
 * @property string|null $email
 * @property string|null $geboortedatum
 * @property string|null $straatnaam
 * @property string|null $huisnummer
 * @property string|null $postcode
 * @property string|null $woonplaats
 * @property string|null $telefoon
 * @property int|null $les_interval
 * @property int|null $les_type
 * @property int|null $les_dag
 * @property string|null $les_tijdstip
 * @property int|null $les_minuten
 * @property float|null $les_tarief
 * @property int $geen_les_meer
 * @property string|null $eerste_les
 * @property string|null $les_ingangsdatum_wijziging
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|\App\Factuur[] $facturen
 * @property-read int|null $facturen_count
 * @property-read Collection|\App\FactuurRecord[] $invoiceRecords
 * @property-read int|null $invoice_records_count
 * @property-read Collection|\App\Vakantie[] $vakanties
 * @property-read int|null $vakanties_count
 * @method static Builder|Leerling newModelQuery()
 * @method static Builder|Leerling newQuery()
 * @method static Builder|Leerling query()
 * @method static Builder|Leerling whereAchternaam($value)
 * @method static Builder|Leerling whereCreatedAt($value)
 * @method static Builder|Leerling whereDocentId($value)
 * @method static Builder|Leerling whereEersteLes($value)
 * @method static Builder|Leerling whereEmail($value)
 * @method static Builder|Leerling whereGeadreseerde($value)
 * @method static Builder|Leerling whereGeboortedatum($value)
 * @method static Builder|Leerling whereGeenLesMeer($value)
 * @method static Builder|Leerling whereHuisnummer($value)
 * @method static Builder|Leerling whereId($value)
 * @method static Builder|Leerling whereLesDag($value)
 * @method static Builder|Leerling whereLesIngangsdatumWijziging($value)
 * @method static Builder|Leerling whereLesInterval($value)
 * @method static Builder|Leerling whereLesMinuten($value)
 * @method static Builder|Leerling whereLesTarief($value)
 * @method static Builder|Leerling whereLesTijdstip($value)
 * @method static Builder|Leerling whereLesType($value)
 * @method static Builder|Leerling whereNaam($value)
 * @method static Builder|Leerling wherePostcode($value)
 * @method static Builder|Leerling whereStraatnaam($value)
 * @method static Builder|Leerling whereTelefoon($value)
 * @method static Builder|Leerling whereUpdatedAt($value)
 * @method static Builder|Leerling whereWoonplaats($value)
 * @mixin \Eloquent
 */
class Leerling extends Model
{
    //
    private const GET_D_M_Y = 'd-m-Y';
    private const SET_Y_M_D = 'Y-m-d';
    private const EERSTE_LES = 'eerste_les';
    private const VERKOOP_TYPE = 'verkoop_type';
    private const GEVALIDEERD = 'gevalideerd';
    private const LES_DATUM = 'les_datum';
    private const LES_INGANGSDATUM_WIJZIGING = 'les_ingangsdatum_wijziging';
    public $table = 'leerling';
    protected $guarded = [];

    /**
     * @var bool
     */
    public $error;
    private $herintrede_datum;


    public function vakanties(): HasMany
    {
        return $this->hasMany(Vakantie::class);
    }

    public function facturen(): HasMany
    {
        return $this->hasMany(Factuur::class);
    }

    public function invoiceRecords(): HasMany
    {
        return $this->hasMany(FactuurRecord::class);
    }

    public function addVakantie(array $vakantie): void
    {
        $this->vakanties()->create($vakantie);
    }

    // ----------Getters----------

    /**
     * @param $value
     * @return string|null
     * @throws Exception
     */
    public function getGeboortedatumAttribute($value): ?string
    {
        if ($value) {
            return (new DateTime($value))->format(self::GET_D_M_Y);
        }
        return null;
    }

    /**
     * @param $value
     * @return string|null
     * @throws Exception
     */
    public function getEersteLesAttribute($value): ?string
    {
        if ($value) {
            return (new DateTime($value))->format(self::GET_D_M_Y);
        }
        return null;
    }

    // ----------Setters----------
    public function setGeboortedatumAttribute($value): void
    {
        if ($value) {
            $this->attributes['geboortedatum'] = date(self::SET_Y_M_D, strtotime($value));
        }
    }

    public function setEersteLesAttribute($value): void
    {
        if ($value) {
            $this->attributes['' . self::EERSTE_LES . ''] = date(self::SET_Y_M_D, strtotime($value));
        }
    }



}
