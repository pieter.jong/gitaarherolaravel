<?php

namespace App\Repositories;

use App\Btw_tarief;
use App\Factuur;
use App\FactuurBtw;
use App\FactuurRecord;
use App\Leerling;

class FactuurRepository
{
    /**
     * @var LeerlingRepository
     */
    private $leerlingRepository;

    public function __construct(LeerlingRepository $leerlingRepository)
    {

        $this->leerlingRepository = $leerlingRepository;
    }

    public function createInvoiceTax(Leerling $leerling, $invoiceRecords, ?bool $invoiceDate = false): array
    {
        [$taxTotalArray, $subtotal, $total] = $this->renderTotals($invoiceRecords);
        $taxRateObjects = $this->createTaxRates($taxTotalArray, $invoiceDate);

        return [
            'btw_tarieven' => $taxRateObjects,
            'subtotaal' => number_format($subtotal, 2),
            'totaal' => number_format($total, 2),
        ];
    }


    public function createInvoice(Leerling $pupil, bool $invoiceDate = false)
    {
        $invoice = new Factuur();
        if (!$pupil->email or !$pupil->geadreseerde) {
            $GLOBALS['error']['geen_email'][] = 'Er is geen email adres opgegeven voor ' . $pupil->naam . ' ' . $pupil->achternaam . '. Hierdoor kan er geen factuur gemaild worden';
            return false;
        }
        $invoiceRecords = $this->leerlingRepository->recordsBasedOnInvoiceSchedule($pupil);
        [$taxTotalArray, $subtotal, $total] = $this->renderTotals($invoiceRecords);
        $taxRateObjects = $this->createTaxRates($taxTotalArray, $invoiceDate);

        // Hoe gaan we het doen als de btw tarieven veranderen? nieuw defaults? of werken met versies.
        if ($invoiceRecords->count()) { // als er geen factuur records zijn, dan ook geen factuur aanmaken.
            $invoice->factuur_nr = $this->createInvoiceNumber();
            $invoice->factuur_datum = ($invoiceDate) ? date('Y-m-d',
                strtotime($invoiceDate)) : date('Y-m-d'); // bij handmatig willen we ook een factuur datum kunnen opgeven
            $invoice->betaal_termijn = 14;
            $invoice->verloop_datum = date('Y-m-d',
                strtotime($invoice->factuur_datum . ' + ' . $invoice->betaal_termijn . ' days'));

            $invoice->subtotaal = $subtotal;
            $invoice->totaal = $total;
            // $this->start_datum = date('Y-m-d'); // kan straks weg

            $invoice->email_contactpersoon = ($pupil->geadreseerde) ? $pupil->geadreseerde : $pupil->email;
            $invoice->pdf_bestand = 'leeg'; // Willen we nog pdf bestanden opslaan op de server? Info zal toch bevroren moeten worden.
            $invoice->verstuurd = 1;

            $invoice->docent_id = auth()->id();
            $pupil->facturen()->save($invoice);
            $invoice->factuur_btws()->saveMany($taxRateObjects);
            $invoice->factuur_records()->saveMany($invoiceRecords);
            return $this;
        }

    }

    public function createInvoiceNumber(): string
    {
        $month = date('Y-m-d');
        $startDate = date('Y-m-d', strtotime('first day of this month'));
        $endDate = date('Y-m-d', strtotime('last day of this month'));
        $invoiceOfThisMonth = Factuur::where('factuur_datum', '>=', $startDate)->where('factuur_datum', '<=',
            $endDate)->get();
        $counter = count($invoiceOfThisMonth) + 1;
        // officieel mag je nooit een factuur verwijderen. Dus verwijderen betekend automatisch een credit factuur sturen voor of de week daarop verekenen.
        $YearMonth = date('Ym', strtotime($month));
        return $YearMonth . sprintf("%03d", $counter);
    }

//    public function scheduledInvoice(){
//        return view('factuur_administratie.');
//    }
    private function renderTotals($invoiceRecords): array
    {
        $subtotal = null;
        $total = null;
        $taxTotalArray = array();
        foreach ($invoiceRecords as $invoiceRecord) {
            $invoiceRecord->gevalideerd = 1;
            $subtotal += $invoiceRecord->qwantiteit * $invoiceRecord->tarief;
            $total += (($invoiceRecord->qwantiteit * $invoiceRecord->tarief) * ($invoiceRecord->btw_tarief / 100)) + ($invoiceRecord->qwantiteit * $invoiceRecord->tarief);
            if (!array_key_exists($invoiceRecord->btw_tarief_id, $taxTotalArray)) {
                $taxTotalArray[$invoiceRecord->btw_tarief_id] = ($invoiceRecord->qwantiteit * $invoiceRecord->tarief) * ($invoiceRecord->btw_tarief / 100);

            } else {
                $taxTotalArray[$invoiceRecord->btw_tarief_id] += ($invoiceRecord->qwantiteit * $invoiceRecord->tarief) * ($invoiceRecord->btw_tarief / 100);
            }
        }
        return [$taxTotalArray, $subtotal, $total];
    }

    private function createTaxRates(array $taxTotalArray, bool $invoiceDate): array
    {
        $taxRateObjects = array();
        foreach ($taxTotalArray as $index => $val) {
            $taxRate = Btw_tarief::find($index);
            $invoiceTax = new FactuurBtw();
            $invoiceTax->omschrijving = $taxRate->omschrijving;
            $invoiceTax->totaal = number_format($val, 2);
            $invoiceTax->percentage = $taxRate->percentage;
            $invoiceTax->btw_tarieven_id = $taxRate->id;
            $invoiceTax->verkoop_datum = ($invoiceDate) ? date('Y-m-d', strtotime($invoiceDate)) : date('Y-m-d');
            $invoiceTax->docent_id = auth()->id();

            $taxRateObjects[] = $invoiceTax;
        }
        return $taxRateObjects;
    }

    public function showInvoiceMonths(Factuur $invoice)
    {
        $monthsArray = [];
        $invoiceRecords = $invoice->factuur_records;
        /** @var FactuurRecord $invoiceRecord */
        foreach ($invoiceRecords as $invoiceRecord) {
            $month = date('M', strtotime($invoiceRecord->les_datum));
//            dd($invoiceRecord->les_datum);
            $monthsArray[$month] = $month;
        }

        return implode(', ', $monthsArray);
    }
}