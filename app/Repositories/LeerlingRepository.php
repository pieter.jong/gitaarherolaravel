<?php


namespace App\Repositories;

use App\Http\Requests\StoreLeerling;
use App\Leerling;
use App\Services\LeerlingService;
use App\Vakantie;
use Illuminate\Database\Eloquent\Collection;

class LeerlingRepository
{
    private const VERKOOP_TYPE = 'verkoop_type';
    private const GEVALIDEERD = 'gevalideerd';
    private const LES_DATUM = 'les_datum';
    private const GET_D_M_Y = 'd-m-Y';
    private const SET_Y_M_D = 'Y-m-d';

    /**
     * @var bool
     */
    public $error;


    /**
     * @var LeerlingService
     */
    private $leerlingService;

    /**
     * LeerlingRepository constructor.
     * @param LeerlingService $leerlingService
     */
    public function __construct(LeerlingService $leerlingService)
    {
        $this->leerlingService = $leerlingService;
    }

    /**
     * @param StoreLeerling $request
     */
    public function store(StoreLeerling $request): void
    {
        $pupil = Leerling::create($request->validated());
        $start_date = $this->leerlingService->checkObligated($pupil);
        if (!$pupil->error) {
            $this->leerlingService->planLessons($pupil, $start_date);
        }
    }

    /**
     * @param Leerling $pupil
     * @return string|null|false
     */
    public function checkObligated(Leerling $pupil)
    {
        return $this->leerlingService->checkObligated($pupil);
    }

    /**
     * @param StoreLeerling $request
     * @param Leerling $pupil
     */
    public function update(StoreLeerling $request, Leerling $pupil): void
    {
        $hasChanges = $this->leerlingService->hasLessonPlannerChanges($pupil);

        $pupil->update($request->validated());

        $startDate = $this->leerlingService->checkObligated($pupil);

        if (!$pupil->error && $hasChanges) {
            $this->leerlingService->planLessons($pupil, $startDate);
        }
    }

    public function recordsBasedOnInvoiceSchedule(Leerling $pupil): Collection
    {
        // Eind_datum moet nog variabel worden. Gebasseerd op betalingsmethode.
        return $pupil->invoiceRecords()
            ->Select('verkoop_producten.*')
            ->addSelect('factuur_record.*')
            ->leftJoin('verkoop_producten', 'verkoop_producten.id', '=', 'factuur_record.verkoop_product_id')
            ->where(self::VERKOOP_TYPE, 1)
            ->where('canceled', null)
            ->where(self::GEVALIDEERD, null)
            ->where(self::LES_DATUM, '<=', date('Y-m-t', strtotime(date(self::SET_Y_M_D) . ' + 1 month')))
            ->get();
    }

    public function cancelAndRestoreLessons(Leerling $pupil, Vakantie $holiday, bool $cancel = false): void
    {
        $this->leerlingService->cancelAndRestoreLessons($pupil, $holiday, $cancel);
    }

//    public function generateLesson(Leerling $pupil, string $startDate, string $endDate): void
//    {
//        $lessonRecords = null;
//        $this->checkObligated($pupil);
//        if (!$pupil->error) {
//            if ($pupil->geen_les_meer === 0) {
//                $startDate = $this->leerlingService->determentStartDateLesson($startDate);
//                if ($startDate) {
//                    $this->leerlingService->
//                    $lessonRecords = $pupil->prepare_lesson_records($startDate, $endDate);
//                }
//            }
//            if ($lessonRecords && !$pupil->error) {
//                $pupil->factuur_records()->saveMany($lessonRecords);
//            }
//        }
//    }

    public function planLessons(Leerling $pupil, $startDate, ?string $endDate = null): bool
    {
        if (!$endDate){
            $endDate = date(self::SET_Y_M_D, strtotime($startDate . ' + 1 year'));
        }
        $this->leerlingService->planLessons($pupil, $startDate, $endDate);
    }
}