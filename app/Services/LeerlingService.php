<?php


namespace App\Services;


use App\Btw_tarief;
use App\FactuurRecord;
use App\Leerling;
use App\Vakantie;

class LeerlingService
{
    private const GET_D_M_Y = 'd-m-Y';
    private const SET_Y_M_D = 'Y-m-d';
    private const EERSTE_LES = 'eerste_les';
    private const VERKOOP_TYPE = 'verkoop_type';
    private const GEVALIDEERD = 'gevalideerd';
    private const LES_DATUM = 'les_datum';
    private const LES_INGANGSDATUM_WIJZIGING = 'les_ingangsdatum_wijziging';
    private $error;

    /**
     * @param Leerling $pupil
     * @return false|string|null
     */
    public function checkObligated(Leerling $pupil)
    {
        $obligated = [];
        //todo waar wordt herintrede_datum geset?
        if ($pupil->herintrede_datum && $pupil->les_dag !== date('N', strtotime($pupil->herintrede_datum))) {

            $obligated['dates_dont_match'] = 'De Lesdag en de datum van de herintrede les komen niet overeen bij. ' . $pupil->naam . ' ' . $pupil->achternaam;
            $this->error = true;
        }
        if (!$pupil->eerste_les) {
            $obligated['geen_eerste_les'] = 'Er is geen "eerste les" opgegeven voor ' . $pupil->naam . ' ' . $pupil->achternaam . '. Het systeem weet hierdoor niet vanaf welke datum er berekend moet worden.';
            $this->error = true;
        }
        if (!$pupil->les_tarief) {
            $obligated['geen_tarief'] = ' Er is geen tarief ingevuld voor ' . $pupil->naam . ' ' . $pupil->achternaam . ' Hierdoor kunnen we geen bedrag berekennen.';
            $this->error = true;
        }
        if (!$pupil->les_minuten) {
            $obligated['geen_les_minuten'] = 'Er zijn geen les_minuten ingevuld voor ' . $pupil->naam . ' ' . $pupil->achternaam . ' Hierdoor kunnen we geen goeie les omschrijving maken.';
            $this->error = true;
        }
        if (!$pupil->email and !$pupil->geadreseerde) {
            $obligated['geen_les_minuten'] = 'Er is geen email adres ingevuld voor ' . $pupil->naam . ' ' . $pupil->achternaam . ' Hierdoor kunnen we geen facturen versturen.';
            $this->error = true;
        }
        $btw_hoog = Btw_tarief::where('default', 1)->first();
        if (!$btw_hoog) {
            $obligated['btw_hoog'] = 'Er is geen "btw hoog tarief" ingevuld bij je "Instellingen".';
            $this->error = true;
        }
        $btw_vrij = Btw_tarief::where('default', 2)->first();
        if (!$btw_vrij) {
            $obligated['btw_vrij'] = 'Er is geen "btw_vrij tarief" ingevuld bij je "Instellingen".';
            $this->error = true;
        }
        if (!$pupil->geboortedatum) {
            $obligated['geboorte_datum'] = 'Er is geen geboortedatum opgegeven voor ' . $pupil->naam . ' ' . $pupil->achternaam . ' Hierdoor weten we niet welk belasting tarief we moeten hanteren';
            $this->error = true;
        }

        $startDate = null;
        // Als er nog geen factuur is verstuur, dan mag eerste_les ook mee tellen.
        if (!count($pupil->facturen()->get())) {
            if (!request(self::EERSTE_LES)) {
                $this->error = true;
            } else {
                $startDate = date(self::SET_Y_M_D, strtotime(request(self::EERSTE_LES)));
            }
        } else {
            if (!request(self::LES_INGANGSDATUM_WIJZIGING)) {
                $obligated[self::LES_INGANGSDATUM_WIJZIGING] = 'Er is geen ingangsdatum opgegeven';
                $this->error = true;
            } else {
                $startDate = date(self::SET_Y_M_D, strtotime(request(self::LES_INGANGSDATUM_WIJZIGING)));
            }
            $last_sold_lesson = $pupil->invoiceRecords()
                ->where(self::VERKOOP_TYPE, 1)
                ->orderBy(self::LES_DATUM, 'desc')->first();
            if ($last_sold_lesson && request('ingangs_datum') < $last_sold_lesson->les_datum) {
                $obligated[self::LES_INGANGSDATUM_WIJZIGING] = 'De ingangsdatum moet groter zijn dan de laatst verkochte les. Groter dan ' . date(self::GET_D_M_Y,
                        strtotime($last_sold_lesson->les_datum));
                $this->error = true;
            }
        }
        if ($this->error === true) {
            $pupil->error = true;
        }
        session()->flash('obligated', $obligated);
        return $startDate;
    }


    public function hasLessonPlannerChanges(Leerling $pupil): bool
    {
        $changed = false;
        if ($pupil->les_interval !== request('les_interval')) {
            $changed = true;
        }
        if ((int)request('les_type') !== $pupil->les_type) {
            $changed = true;
        }
        if ((int)request('les_dag') !== $pupil->les_dag) {
            $changed = true;
        }
        if ((int)request('les_tijdstip') !== $pupil->les_tijdstip) {
            $changed = true;
        }
        if ((int)request('les_minuten') !== $pupil->les_minuten) {
            $changed = true;
        }
        if ((int)request('les_tarief') !== $pupil->les_tarief) {
            $changed = true;
        }
        return $changed;
    }

    private function cancelLessenDuringHoliday(Leerling $pupil, array $newLessons): void
    {
        $holidayArray = [];
        //Todo mak hier even 1 query van.
        $holidays = $pupil->vakanties()->get();
        $docent_vakanties = Vakantie::where('docent_id', auth()->id())
            ->where('vakantie_type', Vakantie::HOLIDAY_TYPE_TEACHER)
            ->get();
        $merged = $holidays->merge($docent_vakanties);

        foreach ($merged as $vakantie) {
            $date = date(self::SET_Y_M_D, strtotime($vakantie->start_datum));
            while ($date <= date(self::SET_Y_M_D, strtotime($vakantie->eind_datum))) {
                $holidayArray[] = $date;
                $date = date(self::SET_Y_M_D, strtotime($date . ' + 1 day'));
            }
        }

        foreach ($newLessons as $nieuwe_les) {
            if (in_array($nieuwe_les->les_datum, $holidayArray)) {
                $nieuwe_les->canceled = 1;
            }
        }
    }


    /**
     * @param Leerling $pupil
     * @param string $startDate
     * @param string $endDate
     * @return bool
     */
    public function planLessons(Leerling $pupil, string $startDate, string $endDate): bool
    {
        if ($pupil->geen_les_meer) {
            $this->removeUnsoldLessons($pupil);
        }

        if (!$this->error) { // Lessen een jaar vooruit plannen.
            $evaluatedStartDate = $this->determentStartDateLesson($pupil, $startDate);

            $this->removeUnsoldLessons($pupil, $evaluatedStartDate);

            $newLessons = $this->prepareNewLessonRecords($pupil, $evaluatedStartDate, $endDate);

            $this->cancelLessenDuringHoliday($pupil, $newLessons);

            $this->createNotificationsForManuallyMovedLessons($pupil, $newLessons, $evaluatedStartDate);

            $pupil->invoiceRecords()->saveMany($newLessons);
        }

        return false;

    }


    /**
     * @param Leerling $pupil
     * @param null $startDate
     * @return false|string|null
     */
    public function determentStartDateLesson(Leerling $pupil, ?string $startDate = null)
    {
        $dateSetter = null;
        $this->checkObligated($pupil); // voor wanneer deze functie wordt aangeroepen zonder dat er controle is geweest op obligated.
        if ($this->error) {
            return false;
        }

        $lastCalculatedLesson = $pupil->invoiceRecords()->orderBy(self::LES_DATUM, 'desc')->first();

        if ($pupil->herintrede_datum) {
            if ($pupil->les_dag == date('N', strtotime($pupil->herintrede_datum))) {
                // Herintrede datum en lesdag mogen geen verschil in lesdagen hebben.
                $startDate = $pupil->les_dag;
            }
        } elseif ($startDate) {
            $startDate = date(self::SET_Y_M_D, strtotime($startDate));
            $diffDays = $pupil->les_dag - $dayNumber = date('N', strtotime($startDate));
            $dateSetter = date(self::SET_Y_M_D, strtotime($startDate . ' ' . $diffDays . ' days'));
            $dateSetter = date(self::SET_Y_M_D, strtotime($dateSetter . '+' . $pupil->les_interval . ' weeks'));
        } elseif ($lastCalculatedLesson) {
            $diffDays = $pupil->les_dag - $dayNumber = date('N', strtotime($lastCalculatedLesson));
            $dateSetter = date(self::SET_Y_M_D, strtotime($lastCalculatedLesson . ' ' . $diffDays . ' days'));
            $dateSetter = date(self::SET_Y_M_D, strtotime($dateSetter . '+' . $pupil->les_interval . ' weeks'));
        } else {
            $diffDays = $pupil->les_dag - $dayNumber = date('N', strtotime($pupil->eerste_les));
            $dateSetter = date(self::SET_Y_M_D, strtotime($pupil->eerste_les . ' ' . $diffDays . ' days'));
        }

        if ($startDate && $dateSetter) {
            while (date('Y-m', strtotime($dateSetter)) < date('Y-m', strtotime($startDate))) {
                $dateSetter = date(self::SET_Y_M_D, strtotime($dateSetter . '+' . $pupil->les_interval . ' weeks'));
            }
        }
        return $dateSetter; // Geen handmatigeDatum? dan wordt berekend vanaf de de laatst_berekende_les of de herintrede_datum.
    }


    /**
     * @param Leerling $pupil
     * @param $startDate
     * @param $endDate
     * @return array|null
     */
    public function prepareNewLessonRecords(Leerling $pupil, $startDate, $endDate): ?array
    {
        $this->checkObligated($pupil); // voor wanneer deze functie wordt aangeroepen zonder dat er controle is geweest op obligated.
        if ($this->error) {
            return false;
        }

        $exceptions = Vakantie::wherein('leerling_id', [$pupil->id, 0])
            ->wherein('vakantie_type', [Vakantie::HOLIDAY_TYPE_PUPIL, Vakantie::HOLIDAY_TYPE_TEACHER])
            ->where('start_datum', '>=', $startDate)
            ->where('eind_datum', '<=', $endDate)
            ->get();

        $plannedLesson = $pupil->invoiceRecords()
            ->where(self::VERKOOP_TYPE, 1)
            ->where(self::LES_DATUM, '>', $startDate)
            ->get();

        $lastDayOfMonth = $endDate;
        $potentialLessonDate = $startDate;
        $lessonRecords = null;
        while ($potentialLessonDate <= $lastDayOfMonth) {
            $lessonException = false;

            foreach ($exceptions as $exception) {
                if ($potentialLessonDate >= $exception->StartDatum && $potentialLessonDate <= $exception->EindDatum) {
                    $lessonException = true;
                }
            }

            foreach ($plannedLesson as $geplande_les) { // Controle op eerder aangemaakte lessen in dezelfde week
                $diffDays = date('N', strtotime($geplande_les->les_datum)) - date('N', strtotime($potentialLessonDate));
                if (date(self::SET_Y_M_D,
                        strtotime($geplande_les->les_datum . ' ' . $diffDays . ' days')) === $potentialLessonDate) {
                    $lessonException = true;
                }
            }

            if (!$lessonException) {
                $invoiceRecord = new FactuurRecord();
                $invoiceRecord->setProperties($pupil, $potentialLessonDate);
                $lessonRecords[] = $invoiceRecord;
            }

            $potentialLessonDate = date(self::SET_Y_M_D,
                strtotime($potentialLessonDate . '+' . $pupil->les_interval . ' weeks'));
        }

        return $lessonRecords;
    }


    /**
     * @param Leerling $pupil
     * @param Vakantie|null $vakantie
     * @param bool $cancel
     */
    public function cancelAndRestoreLessons(Leerling $pupil, ?Vakantie $vakantie = null, bool $cancel = false): void
    {
        // Het nodige werkt nu niet meer.
        // Bijvoorbeeld het verwijderen van een vakantie levert het tegenovergestelde resultaat op.
        // Dit komt omdat we geen start en eind datum meer gebruiken.
        // Hoe gaan we nu lessen uit zetten dan? Bij het aanzetten werkt deze manier prima maar uit is wat lastiger
        // We willen, dat als we een factuurrecord toevoegen/updaten dat deze ook gelijk checkt of dit tijdens een vakantie is.
        // Overigens alleen als het een gitaarles is.
        //$vakantie_array = $this->cancelLessenDuringHoliday(); //Todo Voor deze kwestie moet een andere functie komen.
        //todo $vakantie_array used to be an array of lesson dates to compare with hollidays. This was based on cancelLessenDuringHoliday
        $vakantie_array = [];

        if (request()->start_datum) {
            $start_datum = date(self::SET_Y_M_D, strtotime(request()->start_datum));
            $eind_datum = date(self::SET_Y_M_D, strtotime(request()->eind_datum));
        } elseif ($vakantie) {
            $start_datum = date(self::SET_Y_M_D, strtotime($vakantie->start_datum));
            $eind_datum = date(self::SET_Y_M_D, strtotime($vakantie->eind_datum));
        } else { // start en eind_datum filteren uit vakantie array
            sort($vakantie_array);
            $start_datum = reset($vakantie_array);
            $eind_datum = end($vakantie_array);
            // dd($vakantie_array);

        }
        if ($start_datum) {
            $gitaarlessen = $pupil->invoiceRecords()
                ->where(self::VERKOOP_TYPE, '1')
                // ->wherein('les_datum',$vakantie_array)
                ->where(self::LES_DATUM, '>=', $start_datum)
                ->where(self::LES_DATUM, '<=', $eind_datum)
                ->get();
            foreach ($gitaarlessen as $gitaarles) {
                if (in_array($gitaarles->les_datum, $vakantie_array)) {
                    $gitaarles->canceled = $cancel;
                } else {
                    $gitaarles->canceled = $cancel;
                }
            }
            $pupil->invoiceRecords()->saveMany($gitaarlessen);
        }
    }

    /**
     * @param Leerling $pupil
     * @param null $startDate
     */
    private function removeUnsoldLessons(Leerling $pupil, $startDate = null): void
    {
        if ($startDate) {
            $pupil->invoiceRecords()
                ->where(self::VERKOOP_TYPE, 1)
                // ->where('les_datum','>', $start_date) // Hier moet de vanaf datum nog komen.
                ->where(self::GEVALIDEERD, null)
                ->delete();
        } else {
            $pupil->invoiceRecords()
                ->where(self::VERKOOP_TYPE, 1)
                ->where(self::GEVALIDEERD, null)
                ->delete();
        }
    }

    private function createNotificationsForManuallyMovedLessons(Leerling $pupil, array $newLessons, $startDate): void
    {
        $manuallyMovedLessons = $pupil->invoiceRecords()
            ->where(self::VERKOOP_TYPE, 1)
            ->where('gewijzigd', 1)
            ->where(self::LES_DATUM, '>', $startDate)
            ->get();

        foreach ($manuallyMovedLessons as $m_lesson) {
            $verschil_dagen = date('N', strtotime($m_lesson->les_datum)) - 1;
            $maandag = date(self::SET_Y_M_D, strtotime($m_lesson->les_datum . ' ' . $verschil_dagen . ' days'));
            $zondag = date(self::SET_Y_M_D, strtotime($maandag . ' + 6 days'));
            $nieuwe_les_zelfde_week = $newLessons->where(self::LES_DATUM, '>=', $maandag)->where(self::LES_DATUM, '=<',
                $zondag);
            if ($m_lesson->les_datum !== $nieuwe_les_zelfde_week->les_datum) {
                date('W', strtotime($m_lesson->les_datum));
                date('D', strtotime($m_lesson->les_datum));
                date('D', strtotime($nieuwe_les_zelfde_week->les_datum));

                $GLOBALS['error']['manualy_moved_lessons'][] = 'In week ' . date('W',
                        strtotime($m_lesson->les_datum)) . ' staat een handmatig verschoven les die niet overeen komt met je huidge planning. Controleer de lessen van week ' . date('W',
                        strtotime($m_lesson->les_datum)) . ' via "!Alerts" of bekijk de lessen bij deze leerling';
            }
        }
    }
}