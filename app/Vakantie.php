<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

/**
 * App\Vakantie
 *
 * @property int $id
 * @property int|null $leerling_id
 * @property int $docent_id
 * @property string $start_datum
 * @property string $eind_datum
 * @property string $omschrijving
 * @property int $vakantie_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vakantie newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vakantie newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vakantie query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vakantie whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vakantie whereDocentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vakantie whereEindDatum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vakantie whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vakantie whereLeerlingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vakantie whereOmschrijving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vakantie whereStartDatum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vakantie whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vakantie whereVakantieType($value)
 * @mixin \Eloquent
 */
class Vakantie extends Model
{
    public $table = "vakantie";
    protected $guarded = [];

    public const HOLIDAY_TYPE_PUPIL = 1;
    public const HOLIDAY_TYPE_TEACHER = 2;

    // Getters
    public function getStartDatumAttribute($value)
    {
        if ($value) {
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
    }
    public function getEindDatumAttribute($value)
    {
        if ($value) {
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
    }

    // Setter
    public function setStartDatumAttribute($value)
    {
        if($value){
            $this->attributes['start_datum'] = date('Y-m-d', strtotime($value));
        }
    }

    public function setEindDatumAttribute($value)
    {
        if($value){
            $this->attributes['eind_datum'] = date('Y-m-d', strtotime($value));
        }
    }

    public function validatie_test($validator){
//        $validator = Validator::make();
        $validator->after(function ($validator) {
//            if ($this->somethingElseIsInvalid()) {
            // $validator->errors()->add('joep', 'Vanuit een ander object');
//            }
        });
    }
}
