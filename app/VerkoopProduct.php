<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Btw_tarief;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\VerkoopProduct
 *
 * @property int $id
 * @property int $btw_tarief_id
 * @property int $docent_id
 * @property float $tarief
 * @property string $omschrijving
 * @property float|null $vooraad
 * @property int|null $default
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Btw_tarief $btw_tarief
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerkoopProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerkoopProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerkoopProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerkoopProduct whereBtwTariefId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerkoopProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerkoopProduct whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerkoopProduct whereDocentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerkoopProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerkoopProduct whereOmschrijving($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerkoopProduct whereTarief($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerkoopProduct whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VerkoopProduct whereVooraad($value)
 * @mixin \Eloquent
 */
class VerkoopProduct extends Model
{
    public $table = "verkoop_producten";
    protected $guarded = [];
    public const STATIC_SALES_ITEM_YOUNGER_THAN_21 = 1;
    public const STATIC_SALES_ITEM_OLDER_THAN_21 = 2;

    public function invoiceRecords(): HasMany
    {
        return $this->hasMany(FactuurRecord::class);
    }

    public function btw_tarief()
    {
        return $this->belongsTo('App\Btw_tarief','btw_tarief_id');
    }
}
