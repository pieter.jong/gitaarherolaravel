<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\vakantie_type
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\vakantie_type newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\vakantie_type newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\vakantie_type query()
 * @mixin \Eloquent
 */
class vakantie_type extends Model
{
    //
}
