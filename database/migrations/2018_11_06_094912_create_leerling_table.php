<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeerlingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leerling', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('docent_id');
            $table->string('naam');
            $table->string('achternaam');
            $table->string('geadreseerde')->nullable();
            $table->string('email')->nullable();
            $table->date('geboortedatum')->nullable();
            $table->string('straatnaam')->nullable();
            $table->string('huisnummer')->nullable();
            $table->string('postcode')->nullable();
            $table->string('woonplaats')->nullable();
            $table->string('telefoon')->nullable();
            $table->integer('les_interval')->nullable();
            $table->integer('les_type')->nullable();
            $table->integer('les_dag')->nullable();
            $table->string('les_tijdstip')->nullable();
            $table->integer('les_minuten')->nullable();
            $table->decimal('les_tarief')->nullable();
            $table->boolean('geen_les_meer')->default(0);
            $table->date('eerste_les')->nullable();
            $table->date('les_ingangsdatum_wijziging')->nullable();
            $table->timestamps();

            $table->foreign('docent_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leerling');
    }
}
