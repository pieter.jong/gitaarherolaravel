<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVakantieTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vakantie', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('leerling_id')->nullable();
            $table->unsignedInteger('docent_id');
            $table->date('start_datum');
            $table->date('eind_datum');
            $table->text('omschrijving');
            $table->integer('vakantie_type'); // docent vakantie of leerling vakantie
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vakantie');
    }
}
