<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactuurTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factuur', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('leerling_id');
            $table->unsignedInteger('docent_id');
            $table->string('factuur_nr');
            $table->date('factuur_datum');
            $table->date('verloop_datum');
            $table->decimal('subtotaal');
            $table->decimal('totaal');
            $table->integer('betaal_termijn');
            $table->integer('btw_percentage');  
            $table->string('email_contactpersoon');
            $table->text('pdf_bestand');
            $table->boolean('verstuurd');
            $table->timestamps();

            $table->foreign('docent_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factuur');
    }
}
