<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactuurRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factuur_record', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('factuur_id')->nullable();
            $table->unsignedInteger('btw_tarief_id');
            $table->unsignedInteger('leerling_id');
            $table->unsignedInteger('docent_id');
            $table->unsignedBigInteger('verkoop_product_id')->nullable();
            $table->decimal('qwantiteit');
            $table->integer('btw_tarief');
            $table->decimal('tarief');
            $table->date('verkoop_datum'); // verkoop datum
            $table->integer('les_minuten');
            $table->time('les_tijdstip');
            $table->date('les_datum');
            $table->integer('verkoop_type');
            $table->integer('default')->nullable();
            $table->date('factuur_datum')->nullable();
            $table->boolean('gevalideerd')->nullable();
            $table->boolean('gewijzigd')->nullable();
            $table->boolean('canceled')->nullable();
            $table->timestamps();

            $table->foreign('docent_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factuur_record');
    }
}
