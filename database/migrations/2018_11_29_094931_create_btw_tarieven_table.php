<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBtwTarievenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('btw_tarieven', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('docent_id');
            $table->integer('percentage');
            $table->string('omschrijving');
            $table->integer('default')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('btw_tarieven');
    }
}

// Alleen voor facturen
// omschrijving
// totaal
// percentage
// has_one btw_tarief
// Als we een query willen draaien met alle 0% btw, hoe doen we dat dan?