<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerkoopProductenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verkoop_producten', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('btw_tarief_id');
            $table->unsignedInteger('docent_id');
            $table->decimal('tarief');
            $table->string('omschrijving');
            $table->decimal('vooraad')->nullable(); // leuke optie
            $table->integer('default')->nullable();
            $table->timestamps();

            $table->foreign('docent_id')
                ->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::dropIfExists('verkoop_producten');
    }
}
