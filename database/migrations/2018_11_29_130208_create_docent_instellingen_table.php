<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocentInstellingenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docent_instellingen', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('docent_id');
        $table->string('email')->nullable();
        $table->string('naam')->nullable();
        $table->string('achternaam')->nullable();

        $table->string('bedrijfslogo')->nullable();
        $table->string('bedrijfsnaam')->nullable();
        $table->string('straat')->nullable();
        $table->string('huis_nr')->nullable();
        $table->string('postcode')->nullable();
        $table->string('woonplaats')->nullable();
        $table->string('telefoon')->nullable();
        $table->string('kvk_nr')->nullable();
        $table->string('btw_nr')->nullable();
        $table->string('iban_nr')->nullable();
        $table->string('bedrijfs_email')->nullable();
        $table->string('bedrijfs_website')->nullable();

        $table->integer('default_les_minuten')->nullable();
        $table->decimal('default_les_tarief')->nullable();
        $table->timestamps();

        $table->foreign('docent_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docent_instellingen');
    }
}
