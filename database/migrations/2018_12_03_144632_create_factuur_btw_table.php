<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactuurBtwTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factuur_btw', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('docent_id');
            $table->unsignedInteger('factuur_id');
            $table->unsignedInteger('btw_tarieven_id');
            $table->string('omschrijving');
            $table->decimal('totaal');
            $table->integer('percentage');
            $table->date('verkoop_datum');
            $table->timestamps();

            $table->foreign('docent_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factuur_btw');
    }
}
