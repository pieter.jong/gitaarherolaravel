# GuitarAdventure

Well hi there! This repository holds the code and script
for the GuitarAdventure.

**@programmers at TOV**

This is an old project and the code is messy. To give you an idea of my codeStyle this moment. 
Have a look at Leerling, leerlingController, LeerlingRepository, LeerlingService, StoreLeerling.

Still. this is based on old code witch explains why some variables ar in dutch.

## Setup



To get this project working, follow these steps:

1 - **Git clone this file**

2 - **Go to your folder where you just put all these files.**

3 - **Make sure you have [Composer installed](https://getcomposer.org/download/)
and then run:**

```
composer install
```

You may alternatively need to run `php composer.phar install`, depending
on how you installed Composer.

4 - **Make sure you have [npm installed](https://www.npmjs.com/get-npm)
and then run:**

```
npm install
```



5 - **Create a database with for example phpmyadmin**

6 - **Configure the the .env File**

First, make sure you have an `.env` file (you should).
If you don't, copy `.env.example` to create it.

Next, look at the configuration and make any adjustments you
need - specifically `DATABASE_URL` needs to match with the database you've just created in step 3.

7 - **Run migrations**

```
php artisan migrate:fresh
```

8 - **Start the built-in web server**

You can use Nginx or Apache, but the built-in web server works
great:

```
php artisan serve

or

php artisan serve --port 8001
```

Now check out the site at `http://localhost:8000` or `http://127.0.0.1:8000`
Or with your specific port. 

Have fun!


