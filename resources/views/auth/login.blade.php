@extends('un_auth_layout')

@section('content')
<section class="hero is-fullheight">
    <div class="hero-body">
        <div class="container has-text-centered">
            <div class="column is-4 is-offset-4">
                <h3 class="title has-text-grey">Login</h3>
                <p class="subtitle has-text-grey">Login om verder te gaan.</p>
                <div class="box">
                    <figure class="avatar">
                        <img src="/images/guitaradventure_thumbnail.png">
                    </figure>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="field">
                            <div class="control">
                                <input name="email" id="email" type="email" placeholder="Email" class="input is-large{{ $errors->has('email') ? ' is-danger' : '' }}"  value="{{ old('email') }}" required autofocus>
                            </div>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span> 
                            @endif
                        </div>
                        <div class="field">
                            <div class="control">
                                <input name="password" id="password" type="password" placeholder="Wachtwoord" class="input is-large form-control{{ $errors->has('password') ? ' is-danger' : '' }}" required>
                            </div>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span> 
                            @endif
                        </div>
                        <div class="field">
                            <label class="checkbox">
                  <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old( 'remember') ? 'checked' : '' }}>
                  Ingelogd blijven
                </label>
                        </div>
                        <button class="button is-block is-info is-large is-fullwidth">Login</button>
                    </form>
                </div>
                <p class="has-text-grey">
                    <a href="{{ url('/register') }}">Registreren</a> &nbsp;·&nbsp;
                    <a class="btn btn-link" href="{{ route('password.request') }}">Wachtwoord vergeten</a> &nbsp;·&nbsp;
                    <a href="../">Hulp nodig?</a>
                </p>
            </div>
        </div>
    </div>
</section>
@endsection
