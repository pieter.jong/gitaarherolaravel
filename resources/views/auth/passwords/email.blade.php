@extends('un_auth_layout')

@section('content')



<section class="hero is-fullheight">
    <div class="hero-body">
        <div class="container has-text-centered">
            <div class="column is-4 is-offset-4">
                <h3 class="title has-text-grey">Reset wachtwoord</h3>
                <p class="subtitle has-text-grey">Oepsie wachtwoord vergeten</p>
                <div class="box">
                    <figure class="avatar">
                        <img src="/images/guitaradventure_thumbnail.png">
                    </figure>

                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="field">
                            <div class="control">
                                <input name="email" id="email" type="email" placeholder="Email" class="input is-large{{ $errors->has('email') ? ' is-danger' : '' }}"
                                    value="{{ old('email') }}" required autofocus>
                            </div>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span> 
                            @endif

                        </div>
                        <button class="button is-block is-info is-large is-fullwidth">Verstuur wachtwoord reset link</button>
                    </form>
                </div>
                <p class="has-text-grey">
                    <a href="{{ url('/register') }}">Registreren</a> &nbsp;·&nbsp;
                    <a class="btn btn-link" href="{{ route('password.request') }}">Wachtwoord vergeten</a> &nbsp;·&nbsp;
                    <a class="btn btn-link" href="{{ url('login') }}">Login</a><br>
                    <a href="../">Hulp nodig?</a>
                </p>
            </div>
        </div>
    </div>
</section>
@endsection
