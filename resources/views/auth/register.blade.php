@extends('un_auth_layout')

@section('content')


<section class="hero is-fullheight">
    <div class="hero-body">
        <div class="container has-text-centered">
            <div class="column is-4 is-offset-4">
                <h3 class="title has-text-grey">Registreren</h3>
                <p class="subtitle has-text-grey">Registreer om gebruik te maken van GuitarAdventure</p>
                <div class="box">
                    <figure class="avatar">
                        <img src="/images/guitaradventure_thumbnail.png">
                    </figure>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="field">
                            <div class="control">
                                <input name="name" id="name" type="text" placeholder="Naam" class="input is-large{{ $errors->has('name') ? ' is-danger' : '' }}" value="{{ old('name') }}"
                                    required autofocus> 
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span> 
                                @endif
                            </div>
                        </div>
                        <div class="field">
                            <div class="control">
                                <input name="email" id="email" type="email" placeholder="Email" class="input is-large{{ $errors->has('email') ? ' is-danger' : '' }}"
                                    value="{{ old('email') }}" required autofocus>
                            </div>
                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span> @endif
                        </div>
                        <div class="field">
                            <div class="control">
                                <input name="password" id="password" type="password" placeholder="Wachtwoord" class="input is-large {{ $errors->has('password') ? ' is-danger' : '' }}"
                                    required>
                            </div>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span> 
                            @endif
                        </div>
                        <div class="field">
                            <div class="control">
                                <input placeholder="Wachtwoord bevestiging" id="password-confirm" type="password" class="input is-large" name="password_confirmation" required>
                            </div>
                        </div>

                        <button class="button is-block is-info is-large is-fullwidth">Registreer</button>
                    </form>
                </div>
                <p class="has-text-grey">
                    <a href="{{ url('/register') }}">Registreren</a> &nbsp;·&nbsp;
                    <a class="btn btn-link" href="{{ route('password.request') }}">Wachtwoord vergeten</a> &nbsp;·&nbsp;
                    <a class="btn btn-link" href="{{ url('login') }}">Login</a><br>
                    <a href="../">Hulp nodig?</a>
                </p>
            </div>
        </div>
    </div>
</section>









