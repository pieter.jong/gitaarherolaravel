@extends('layout')

@section('content')

    <h1 class="title">btw tarief aanmaken</h1>
    <form id="btw_tarief_create" action="/btwtarief" method="post">
        {{csrf_field()}}
        <div class="section">
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="percentage">Percentage</label>
                        <div class="control">
                            <input id="percentage" name="percentage" class="input {{$errors->has('percentage') ? 'is-danger' : ''}}" type="text" placeholder="percentage" value="{{ old('percentage') }}"/>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="omschrijving">Omschrijving</label>
                        <div class="control">
                            <input id="omschrijving" name="omschrijving" class="input {{$errors->has('omschrijving') ? 'is-danger' : ''}}" type="text" placeholder="omschrijving" value="{{ old('omschrijving') }}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="columns">
        <div class="column">
            <button form="btw_tarief_create" class="button is-primary">Opslaan</button>
            <a href="/instellingen">
                <div class="button">Terug</div>
            </a>
        </div>
    </div>

    @include('errors')

@endsection