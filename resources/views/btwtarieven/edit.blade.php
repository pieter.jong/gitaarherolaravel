@extends('layout')

@section('content')

    <h1 class="title">btw tarief bewerken</h1>
    <form id="btw_tarief_create" action="/btwtarief/{{$btw_tarief->id}}" method="post">
        {{csrf_field()}}
        {{method_field('PATCH')}}
        <div class="section">
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="percentage">Percentage</label>
                        <div class="control">
                            <input id="percentage" name="percentage" class="input {{$errors->has('percentage') ? 'is-danger' : ''}}" type="text" placeholder="percentage" value="{{$errors->has('percentage') ? old('percentage') : $btw_tarief->percentage}}"/>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="omschrijving">Omschrijving</label>
                        <div class="control">
                            <input id="omschrijving" name="omschrijving" class="input {{$errors->has('omschrijving') ? 'is-danger' : ''}}" type="text" placeholder="omschrijving" value="{{$errors->has('omschrijving') ? old('omschrijving') : $btw_tarief->omschrijving}}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="columns">
        <div class="column">
            <button form="btw_tarief_create" class="button is-primary">Opslaan</button>
            <a href="/instellingen">
                <div class="button">Terug</div>
            </a>
        </div>
    </div>

    @include('errors')

@endsection