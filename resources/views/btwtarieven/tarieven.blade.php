<div class="box">
    <div class="level">
        <div class="is-grouped">
            <h3 class="subtitle is-inline">btw tarieven</h3>
            <div class="control is-inline">
                <a href="/btwtarief/create" class="button has-background-grey-light">Aanmaken &nbsp;<i class="far fa-plus-square"></i></a>
            </div>
        </div>
    </div>
    
    @foreach($btw_tarieven as $tarief)
        <div class="columns is-mobile {{ ($loop->iteration % 2 == 0) ? 'has-background-light' : 'has-background-grey-lighter' }} ">
            <div class="column is-2">{{ $tarief->omschrijving }}</div>
            <div class="column is-1-tablet is-4-mobile">{{ $tarief->percentage }}</div>
            <div class="column is-two-quarter is-primary" style="padding:0;">
                <div class="level-right">
                    <a class="button is-primary" href="/btwtarief/{{$tarief->id}}/edit">Bewerken &nbsp;<i class="fas fa-pencil-alt"></i></a>
                </div>
            </div>
        </div>
    @endforeach
</div>