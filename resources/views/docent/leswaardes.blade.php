@extends('layout')

@section('content')

    <h1 class="title">Docent Instellingen</h1>
    <div class="columns">
        <div class="column">
            {{--<button form="leerlingen_edit" class="button is-primary">Update</button>--}}
            <a href="/instellingen">
                <div class="button">Terug</div>
            </a>
        </div>
    </div>
    <form id="docent_leswaardes_edit" action="/instellingen/leswaardes/" method="post">
        {{csrf_field()}}
        {{method_field('PATCH')}}
        <div class="section">
            <h2 class="subtitle">Standaard les instellingen</h2>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="default_les_minuten">Standaard aantal minuten les</label>

                        <div class="control">
                            <input id="default_les_minuten" name="default_les_minuten" class="input" type="text" placeholder="default_les_minuten"
                                   value="{{$docent_instellingen->default_les_minuten}}"/>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="default_les_tarief">Standaard tarief</label>

                        <div class="control">
                            <input id="default_les_tarief" name="default_les_tarief" class="input" type="text" placeholder="default_les_tarief"
                                   value="{{$docent_instellingen->default_les_tarief}}"/>
                        </div>
                    </div>
                </div>
            </div>
            {{--<h2 class="subtitle">Factuur instellingen</h2>--}}
            {{--<div class="columns">--}}
                {{--<div class="column">--}}
                    {{--<div class="field">--}}
                        {{--<label class="label" for="les_dag">Flexibele/vaste betalingsmethode</label>--}}
                        {{--<div id="les_dag" class="select">--}}
                            {{--<select name="les_dag">--}}
                                {{--<option value="1" {{$docent_instellingen->les_dag == 1 ? 'selected' : ''}} >Per leerling</option>--}}
                                {{--<option value="2" {{$docent_instellingen->les_dag == 2 ? 'selected' : ''}} >Hetzelfde voor alle leerlingen</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="column">--}}
                    {{--<div class="field">--}}
                        {{--<label class="label" for="les_dag">Betaling methode</label>--}}
                        {{--<div id="les_dag" class="select">--}}
                            {{--<select name="les_dag">--}}
                                {{--<option value="1" {{$docent_instellingen->les_dag == 1 ? 'selected' : ''}} >vast bedrag per maand</option>--}}
                                {{--<option value="2" {{$docent_instellingen->les_dag == 2 ? 'selected' : ''}} >1 maand vooruit rekenen</option>--}}
                                {{--<option value="3" {{$docent_instellingen->les_dag == 3 ? 'selected' : ''}} >maandelijks achteraf berekenen</option>--}}
                                {{--<option value="4" {{$docent_instellingen->les_dag == 4 ? 'selected' : ''}} >factuur per jaar</option>--}}
                                {{--<option value="5" {{$docent_instellingen->les_dag == 5 ? 'selected' : ''}} >factuur per halfjaar</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </form>
    <div class="columns">
        <div class="column">
            <button form="docent_leswaardes_edit" class="button is-primary">Update</button>
            <a href="/instellingen">
                <div class="button">Terug</div>
            </a>
        </div>
    </div>

    @include('errors')

@endsection