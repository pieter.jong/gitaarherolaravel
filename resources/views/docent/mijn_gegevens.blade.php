@extends('layout')

@section('content')

    <h1 class="title">Docent Instellingen</h1>
    <div class="columns">
        <div class="column">
            {{--<button form="leerlingen_edit" class="button is-primary">Update</button>--}}
            <a href="/instellingen">
                <div class="button">Terug</div>
            </a>
        </div>
    </div>
    <form id="docent_instellingen_edit" action="/instellingen/docentgegevens/" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PATCH')}}
        <div class="section">
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="title">Naam</label>
                        <div class="control">
                            <input id="title" name="naam" class="input {{$errors->has('naam') ? 'is-danger' : ''}}" type="text" placeholder="Naam" value="{{$errors->has('naam') ? old('naam') : $docent_instellingen->naam}}"/>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="achternaam">Achternaam</label>
                        <div class="control">
                            <input id="achternaam" name="achternaam" class="input {{$errors->has('achternaam') ? 'is-danger' : ''}}" type="text" placeholder="achternaam" value="{{$errors->has('achternaam') ? old('achternaam') : $docent_instellingen->achternaam}}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-half">
                    <div class="field">
                        <label class="label" for="email">E-mail</label>
                        <div class="control">
                            <input id="email" name="email" class="input" type="text" placeholder="E-mail" value="{{$docent_instellingen->email}}"/>
                        </div>
                    </div>
                </div>
            </div>

            <h2 class="subtitle">Adres gegevens</h2>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="straat">straat</label>

                        <div class="control">
                            <input id="straat" name="straat" class="input" type="text" placeholder="straat"
                                   value="{{$docent_instellingen->straat}}"/>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="huis_nr">huis_nr</label>

                        <div class="control">
                            <input id="huis_nr" name="huis_nr" class="input" type="text" placeholder="huis_nr"
                                   value="{{$docent_instellingen->huis_nr}}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="postcode">postcode</label>

                        <div class="control">
                            <input id="postcode" name="postcode" class="input" type="text" placeholder="postcode"
                                   value="{{$docent_instellingen->postcode}}"/>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="woonplaats">woonplaats</label>

                        <div class="control">
                            <input id="woonplaats" name="woonplaats" class="input" type="text" placeholder="woonplaats"
                                   value="{{$docent_instellingen->woonplaats}}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-half">
                    <div class="field">
                        <label class="label" for="telefoon">telefoon</label>

                        <div class="control">
                            <input id="telefoon" name="telefoon" class="input" type="text" placeholder="telefoon"
                                   value="{{$docent_instellingen->telefoon}}"/>
                        </div>
                    </div>
                </div>
            </div>
            <h2 class="subtitle">Bedrijfsgegevens</h2>
            <div class="columns">
                <div class="column">
                    <div class="file is-boxed">
                        <label class="file-label">
                            <input style="display: none" class="file-input" type="file" name="bedrijfslogo">
                            <span class="file-cta">
                            <span class="file-icon">
                                <i class="fas fa-upload"></i>
                            </span>
                            <span class="file-label">
                                Upload je bedrijfslogo…
                            </span>
                            </span>
                        </label>
                    </div>
                </div>
                @if ($docent_instellingen->bedrijfslogo)
                    <div class="column">
                        <figure class="image">
                            <img style="width: 200px" src="{{$docent_instellingen->bedrijfslogo}}">
                        </figure>
                    </div>
                @else
                    <p>{{__('No logo uploaded yes')}}</p>
                @endif

            </div>
            
            
            <div class="columns">
                <div class="column is-half">
                    <div class="field">
                        <label class="label" for="bedrijfsnaam">bedrijfsnaam</label>

                        <div class="control">
                            <input id="bedrijfsnaam" name="bedrijfsnaam" class="input" type="text" placeholder="bedrijfsnaam"
                                   value="{{$docent_instellingen->bedrijfsnaam}}"/>
                        </div>
                    </div>
                </div>
                <div class="column is-half">
                    <div class="field">
                        <label class="label" for="kvk_nr">kvk_nr</label>

                        <div class="control">
                            <input id="kvk_nr" name="kvk_nr" class="input" type="text" placeholder="kvk_nr"
                                   value="{{$docent_instellingen->kvk_nr}}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-half">
                    <div class="field">
                        <label class="label" for="btw_nr">btw_nr</label>

                        <div class="control">
                            <input id="btw_nr" name="btw_nr" class="input" type="text" placeholder="btw_nr"
                                   value="{{$docent_instellingen->btw_nr}}"/>
                        </div>
                    </div>
                </div>
                <div class="column is-half">
                    <div class="field">
                        <label class="label" for="iban_nr">iban_nr</label>

                        <div class="control">
                            <input id="iban_nr" name="iban_nr" class="input" type="text" placeholder="iban_nr"
                                   value="{{$docent_instellingen->iban_nr}}"/>
                        </div>
                    </div>
                </div>
            </div>
            <h2 class="subtitle">Standaard les instellingen</h2>
            {{-- <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="les_minuten">Standaard aantal minuten les</label>

                        <div class="control">
                            <input id="les_minuten" name="les_minuten" class="input" type="text" placeholder="les_minuten"
                                   value="{{$docent_instellingen->les_minuten}}"/>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="les_tarief">Standaard tarief</label>

                        <div class="control">
                            <input id="les_tarief" name="les_tarief" class="input" type="text" placeholder="les_tarief"
                                   value="{{$docent_instellingen->les_tarief}}"/>
                        </div>
                    </div>
                </div>
            </div> --}}
            {{--<h2 class="subtitle">Factuur instellingen</h2>--}}
            {{--<div class="columns">--}}
                {{--<div class="column">--}}
                    {{--<div class="field">--}}
                        {{--<label class="label" for="les_dag">Flexibele/vaste betalingsmethode</label>--}}
                        {{--<div id="les_dag" class="select">--}}
                            {{--<select name="les_dag">--}}
                                {{--<option value="1" {{$docent_instellingen->les_dag == 1 ? 'selected' : ''}} >Per leerling</option>--}}
                                {{--<option value="2" {{$docent_instellingen->les_dag == 2 ? 'selected' : ''}} >Hetzelfde voor alle leerlingen</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="column">--}}
                    {{--<div class="field">--}}
                        {{--<label class="label" for="les_dag">Betaling methode</label>--}}
                        {{--<div id="les_dag" class="select">--}}
                            {{--<select name="les_dag">--}}
                                {{--<option value="1" {{$docent_instellingen->les_dag == 1 ? 'selected' : ''}} >vast bedrag per maand</option>--}}
                                {{--<option value="2" {{$docent_instellingen->les_dag == 2 ? 'selected' : ''}} >1 maand vooruit rekenen</option>--}}
                                {{--<option value="3" {{$docent_instellingen->les_dag == 3 ? 'selected' : ''}} >maandelijks achteraf berekenen</option>--}}
                                {{--<option value="4" {{$docent_instellingen->les_dag == 4 ? 'selected' : ''}} >factuur per jaar</option>--}}
                                {{--<option value="5" {{$docent_instellingen->les_dag == 5 ? 'selected' : ''}} >factuur per halfjaar</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </form>
    <div class="columns">
        <div class="column">
            <button form="docent_instellingen_edit" class="button is-primary">Update</button>
            <a href="/instellingen">
                <div class="button">Terug</div>
            </a>
        </div>
    </div>

    @include('errors')

@endsection