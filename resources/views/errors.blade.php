@if($errors->any())
<div id="error-overlay" style="position:fixed; left:0; top:0; width:100%;height:100%;background:rgb(0, 0, 0,0.8); z-index:999;">
    <div style="width:90%; margin:5% auto 0 auto;">
        <div class="notification is-danger is-overlay">
            <a class="delete is-large alert-messages"></a>
            <ul>
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
    
@endif