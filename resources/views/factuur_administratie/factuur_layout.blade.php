<!DOCTYPE html>
<html>
    <head>
            <meta charset='utf-8'>
        <style>
            body{font-size: 13px;color: black;}
                p{margin: 0;}
                table p{margin:0;}
                body, html{
                    font-family:helvetica;
                }
                .klant-gegevens-titel p,.diensten-titel-bar p{
                    padding: 4px;
                }
            .logo{display: inline; overflow: auto; width: 60%}
            .bedrijfs-gegevens{  float: right; width: 40%;}
            .onderwerp{ display:block; width: 100%; margin-top: 20px;}


            .klant-gegevens-titel{display:block;width: 100%; font-weight: 700; background: #99cc33}
            .klant-gegevens{display:block;width: 100%;}

            .diensten{display:block; width: 100%;}
            .diensten-table{width: 100%;}
            .diensten-titel-bar{background: #99cc33}
            .totaal-break-line{display: block; width:100%; background: black;}
            .totaal{display: block; width: 100%;}
            .totaal-tabel{width: 100%;}
            .footer{ margin-top: 80px; display: block; width: 100%; text-align: center}
        </style>
    </head>
    <body>
        <div style="display:block; overflow:auto;">
            <div class="logo">
                <img style="" src="{{$logo}}">
            </div>
            <div class="bedrijfs-gegevens">
                <table>
                    <tr>
                        <td style='text-align:right; width:80px;'>Adres:</td>
                        <td>{{$instellingen->bedrijfsnaam}}<td>
                    </tr>
                    <tr><td></td><td>{{$instellingen->straat}} {{$instellingen->huis_nr}}</td></tr>
                    <tr><td></td><td>{{$instellingen->postcode}} {{$instellingen->woonplaats}}</td></tr>
                    <tr><td style='text-align:right'>T:</td><td>{{$instellingen->telefoon}}</td></tr>
                    {{-- <tr><td style='text-align:right'>I:</td><td>{{$instellingen->bedrijfs_website}}</td></tr> --}}
                    {{-- <tr><td style='text-align:right'>E:</td><td>{{$instellingen->bedrijfs_email}}</td></tr> --}}
                    <tr><td style='text-align:right'>KvK nr:</td><td>{{$instellingen->kvk_nr}}</td></tr>
                    <tr><td style='text-align:right'>Btw nr:</td><td>{{$instellingen->btw_nr}}</td></tr>
                    <tr><td style='text-align:right;'>Rek nr:</td><td>{{$instellingen->iban_nr}}</td></tr>
                </table>
            </div>
        </div>

        <div class="onderwerp">
            <div class="left" style="display: inline-block; width:79%">
                <p style='font-weight:bold;'>Omschrijving:</p>
                <p>Gitaarles maand {{ $invoiceMonths }}</p>
            </div>
            <div class="right" style="display:inline-block;">
                <table>
                    <tr>
                        <td style="text-align:right"><p>Factuur nr:</p></td>
                        <td><p>{{$factuur->factuur_nr}}</p></td>
                    </tr>
                    <tr>
                        <td style="text-align:right"><p>Datum:</p></td>
                        <td><p>{{$factuur->factuur_datum}}</p></td>
                    </tr>
                </table>
                
                
            </div>
        </div>
        
        <div class='klant-gegevens-titel'><p>Klant Gegevens</p></div>
        <div class='klant-gegevens'>
            <table>
                <tr>
                    <td><p>Naam:</p></td>
                    <td><p>{{$leerling->naam}} {{$leerling->achternaam}}</p></td>
                </tr>
                <tr>
                    <td style="vertical-align:text-top">Adres:</td>
                    <td>
                        <p>{{$leerling->straatnaam}} {{$leerling->huis_nr}} {{$leerling->toevoeging}}</p>
                        <p>{{$leerling->postcode}}</p>
                        <p>{{$leerling->woonplaats}}</p>
                    </td>
                </tr>
            </table>
        </div>

        <div class='diensten'>
            <table class='diensten-table'>
                <tr class='diensten-titel-bar'>
                    <td style='width:60%;'><p>Omschrijving</p></td>
                    <td style='width:20%;'><p>Datum</p></td>
                    <td style='width:10%;'><p>Aantal</p></td>
                    <td style='width:10%;'><p>Kosten</p></td>
                </tr>
                @foreach ($invoiceRecords as $record)
                <tr>
                    <td>{{$record->salesItem->omschrijving}}</td>
                    <td>{{($record->les_datum) ? \Carbon\Carbon::parse($record->les_datum)->format('d-m-Y') : ''}}</td>
                    <td >{{ $record->qwantiteit }}</td>
                    <td>&euro; {{$record->tarief}}</td>
                </tr>
                @endforeach
            </table>
        </div>
        <hr class="totaal-break-line">
        <div class='totaal'>
                <table class='totaal-tabel'>
                    <tr><td style="text-align:right; width:90%">Sub Totaal :</td><td> &euro; {{$factuur->subtotaal}}</td></tr>
                    @foreach ($btw as $btw_tarief)
                        <tr><td style="text-align:right">{{$btw_tarief->omschrijving}} :</td><td> &euro; {{$btw_tarief->totaal}} </td></tr>
                    @endforeach
                    <tr><td style="text-align:right">Totaal :</td><td> &euro; {{$factuur->totaal}} </td></tr>
                </table>
        </div>

        <div class='footer'>	
            <p>Graag deze factuur binnen {{$factuur->betaal_termijn}} dagen overmaken op</p>
            <p>{{$instellingen->iban_nr}} T.A.V. {{$instellingen->naam}} {{$instellingen->achternaam}}</p>
            <p>Onder vermelden van factuur nr: {{$factuur->factuur_nr}}</p>
        </div>
    </body>
</html>