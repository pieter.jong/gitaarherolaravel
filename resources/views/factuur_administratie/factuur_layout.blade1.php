{{-- @extends('layout') --}} 

{{-- @section('content') --}}

    {{-- <h1 class="title">Factuur Administratie</h1>
    <div class="box">
        <div class="level">
            <h2 style="text-align:right" class="subtitle">Handmatig facturen verzenden</h2>
        </div>
        

        <div class="columns">
            <div class="column">
            </div>
        </div>

        @foreach ($leerlingen as $leerling)
            <p style="color:aqua">{{$leerling->naam}}</p>
        @endforeach

        
    </div>

    <div class="box">
        <div class="level">
            <h2 class="subtitle"><span class="has-text-primary"><i class="fas fa-check"></i></span> Verzonden facturen</h2>
        </div>
            </div>
    <div class="box">
        <div class="is-grouped">
            <h2 class="subtitle is-inline"><span class="has-text-danger"><i class="fas fa-exclamation-triangle"></i></span> Niet gefactureerde lessen</h2>
            <div class="control">
            </div>
        </div>
        
    </div>
    <div class="box">
        <div class="level">
            <h2 class="subtitle">kwartaal overzicht</h2>
        </div>
    </div> --}}

  
{{-- @endsection

<html>
    <head>
        <style>
            html{
                font-family:helvetica;

            }
            body{
                font-family:helvetica;

            }
            h1{

                font-size:13px;
            }
            p{
                padding:2px 0;
                margin:0px;
                font-size:15px;
            }
            #logo{
                float:right;
                height:300px;
                width:450px;
            }
            #logo img{
                margin-top:30px;
            }
            #mijn-gegevens{
                position:absolute;
                width:300px;
                top:50px;
                right:0px;
            }
            #mijn-gegevens td{
                margin:0;
                padding:5px 0 0 0;
            }

            #factnr{
                position:absolute;
                width:300px;
                right:0px;
                top:300;
            }
            #factnr p{
                padding:5px 0;
                margin:0;
            }
            #onderwerp{
                height:10px;
                width:450px;
            }
            #klant-gegevens-titel{
                width:100%;
                height:30px;
                margin-top:50px;
                background:#99cc33;
                font-size:18px;
            }

            #klant-gegevens-titel p{
                font-size:18px;
                margin:3px 0 0 6px;
            }
            #klant-gegevens{
                margin:10px 0 0 50px;
            }

            #diensten{
                margin-top:50px;
                font-size:18px;
            }
            #diensten td{
                padding:10px 5px;
            }
            #titel-bar{
                background:#99cc33;
            }
            #footer{
                position:absolute;
                top:1010px;
                width:100%;
                text-align:center;
            }
            #totaalbedrag{
                margin-left:545px;
                font-size:18px;
            }
            #totaal-tabel{
                text-align:right;
            }
        </style>
    </head>
    <body>
    <%--<div id='Logo'><img src="file:$server_root/mysite/html2pdf/Afbeeldingen/Logo6.png"></div>--%>
    <div id='Logo'><img src="$server_root/mysite/html2pdf/Afbeeldingen/Logo6.png"></div>
    <div id='mijn-gegevens'>
        <table id='mijn-gegevens-tabel'>
            <tr><td style='text-align:right; width:80px; padding:3px;'><p>Adres:</p></td>
                <td><p style='padding:0px;'>Gitaarhero</p>
                    <p style='padding:0px;'>De skans 15</p>
                    <p style='padding:0px;'>8941DM Leeuwarden</p>
                </td>
            </tr>
            <tr><td style='text-align:right'><p>T:</p></td><td><p>06 52 650 732</p></td></tr>
            <tr><td style='text-align:right'><p>E:</p></td><td><p>pieter@gitaarhero.nl</p></td></tr>
            <tr><td style='text-align:right'><p>I:</p></td><td><p>www.gitaarhero.nl</p></td></tr>
            <tr><td style='text-align:right'><p>KvK nr:</p></td><td><p>59313072</p></td></tr>
            <tr><td style='text-align:right'><p>Btw nr:</p></td><td><p>109541054B02</p></td></tr>
            <tr><td style='text-align:right;'><p>Rek nr:</p></td><td><p>NL45ABNA0609178180</p></td></tr>
        </table>
    </div>
    <div id='factnr'>
        <table>
            <tr><td style='text-align:right; width:90px;'><p>factuur nr:</p></td><td><p>$fact_nr</p></td></tr>
            <tr><td style='text-align:right;'><p>Datum:</p></td><td><p>$date_stamp</p></td></tr>
        </table>
    </div>
    <div id='onderwerp'>
        <table>
            <tr>
                <td><p style='font-weight:bold;'>Factuur:</p></td>
                <td><p>Gitaarles maand $invoice_month</p></td>
            </tr>
        </table>
    </div>
    <div id='klant-gegevens-titel'><p>Klant Gegevens</p></div>
        <% with $leerling %>
        <div id='klant-gegevens'>
            <table>
                <tr>
                    <td><p>Naam:</p></td>
                    <td><p>$naam $achternaam</p></td>
                </tr>
                <tr>
                    <td><p>Adres:</p></td>
                    <td>
                        <p>$straatnaam $huis_nr$toevoeging</p>
                        <p>$postcode</p>
                        <p>$woonplaats</p>
                    </td>
                </tr>
            </table>
        </div>
        <% end_with %>

    <div id='diensten'>
        <table id='diensten-table'>
            <tr id='titel-bar'>
                <td style='width:355px;'>Omschrijving</td>
                <td style='width:160px;'>Datum</td>
                <td style='width:50px;'>Aantal</td>
                <td style='width:100px;'>Kosten</td>
            </tr>
            <% loop $invoice_records %>
                <tr>
                    <td>$omschrijving</td>
                    <td>$les_datum.format('d-m-Y')</td>
                    <td style='text-align:center'>1x</td>
                    <td>&euro; $tarief</td>
                </tr>
            <% end_loop %>
        <%--Hier komt weer een stukje php om de hoek kijken.--%>

            <%--$btw_bedrag = $subTotaal[0] * "0.$BtwTarief";--%>
            <%--$totaal = $subTotaal[0] + $BtwBedrag;--%>
        </table>
        <%--Hier houd het weer op gaat bljkbaar alleen om looplessons--%>
        <hr style='margin:30px 0;'>
    </div>
    <div id='totaal'>
            <span id='totaalbedrag'>
                <table id='totaal-tabel'>
                    <tr><td>Sub Totaal :</td><td> &euro; $subtotaal</td></tr>
                    <tr><td>Btw $btw_tarief% :</td><td> &euro; $btw_bedrag </td></tr>
                    <tr><td>Totaal :</td><td> &euro; $totaal </td></tr>
                </table>
            </span>
    </div>
    <div id='footer'>	<p>Het factuur nr vermelden in de omschrijving</p>
        <p>Graag deze factuur binnen 2 weken overmaken T.A.V.</p>
        <p>P. de Jong NL45ABNA0609178180</p>

    </div>
    </body>
</html>