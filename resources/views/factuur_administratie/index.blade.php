@extends('layout')

@section('content')

    <h1 class="title">Factuur Administratie</h1>

    <div class="box">
        <div class="level">
            <h2 class="subtitle"><span class="has-text-primary"><i class="fas fa-check"></i></span> Verzonden facturen</h2>
        </div>
        @foreach ($facturen as $factuur)
            <p>{{$factuur->factuur_nr}}</p>
        @endforeach
    </div>
    <div class="box">
        <div class="is-grouped">
            <h2 class="subtitle is-inline"><span class="has-text-danger"><i class="fas fa-exclamation-triangle"></i></span> Geplande facturen</h2>
            <div class="control">
                <a href="/generate_invoice/" class="button has-background-grey-light">Maak facturen van onderstaande lessen. &nbsp;<i class="far fa-plus-square"></i></a>
            </div>
        </div>
    </div>
    @include('errors')
@endsection