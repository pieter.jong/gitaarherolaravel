@extends('layout')

@section('content')

<h1 class="title">Komende factuur</h1>
<p>{{$leerling->naam}} wordt "maandelijks vooraf" gefactureerd.</p>
<p>Deze factuur wordt automatisch verzonden op ...</p>   

<div class="box">
    <div class="level">
        <div class="level-left">
            <img style="" src="{{$logo}}">
        </div>
        <div class="level-right">
            <table class="table">
                <tr>
                    <td></td>
                    <td>{{$instellingen->bedrijfsnaam}}
                        <td>
                </tr>
                <tr>
                    <td>Adres:</td>
                    <td>{{$instellingen->straat}} {{$instellingen->huis_nr}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{$instellingen->postcode}} {{$instellingen->woonplaats}}</td>
                </tr>
                </tr>
                <tr>
                    <td>T:</td>
                    <td>{{$instellingen->telefoon}}</td>
                </tr>
                {{--
                <tr>
                    <td style='text-align:right'>I:</td>
                    <td>{{$instellingen->bedrijfs_website}}</td>
                </tr> --}} {{--
                <tr>
                    <td style='text-align:right'>E:</td>
                    <td>{{$instellingen->bedrijfs_email}}</td>
                </tr> --}}
                <tr>
                    <td>KvK nr:</td>
                    <td>{{$instellingen->kvk_nr}}</td>
                </tr>
                <tr>
                    <td>Btw nr:</td>
                    <td>{{$instellingen->btw_nr}}</td>
                </tr>
                <tr>
                    <td>Rek nr:</td>
                    <td>{{$instellingen->iban_nr}}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="level">
        <div class="level-left">
            <div class="columns">
                <div class="column is-5"><strong>Omschrijving</strong></div>
                <div class="column is-12">Gitaarles maand invoice_month</div>
            </div>
        </div>
        <div class="level-right">
            <table class="table">
                <tr>
                    <td>
                        <p>Factuur nr:</p>
                    </td>
                    <td>
                        <p>{{$factuur->factuur_nr}}</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>Datum:</p>
                    </td>
                    <td>
                        <p>{{$factuur->factuur_datum}}</p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <article class="message">
        <div class="message-header">
            <p>Klant gegevens</p>
        </div>
        <div class="message-body">
            <table class="table">
                <tr>
                    <td>
                        <p>Naam:</p>
                    </td>
                    <td>
                        <p>{{$leerling->naam}} {{$leerling->achternaam}}</p>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align:text-top">Adres:</td>
                    <td>
                        <p>{{$leerling->straatnaam}} {{$leerling->huis_nr}} {{$leerling->toevoeging}}</p>
                        <p>{{$leerling->postcode}}</p>
                        <p>{{$leerling->woonplaats}}</p>
                    </td>
                </tr>
            </table>
        </div>
    </article>

    <article class="message">
        <div class="message-header">
            <p>Verkoop producten</p>
        </div>
        <div class="message-body">
            @include('factuur_records.lessen')
        </div>
    </article>
<hr style="background:black;height:2px;">
<div class="columns is-mobile">
    <div class="column is-7"></div>
    <div class="column is-2"><strong>Sub totaal</strong></div>
    <div class="column is-1"><strong>&euro; {{$subtotaal}}</strong></div>
</div>
@foreach ($btw_tarieven as $item)
<div class="columns is-mobile">
    <div class="column is-7"></div>
<div class="column is-2"><strong>{{$item->omschrijving}}</strong></div>
<div class="column is-1"><strong>&euro; {{$item->totaal}}</strong></div>
</div>    
@endforeach

<div class="columns is-mobile">
    <div class="column is-7"></div>
    <div class="column is-2"><strong>Totaal</strong></div>
    <div class="column is-1"><strong>&euro; {{$totaal}}</strong></div>
</div>
    



<a href="/m_confirm_invoice{{$leerling->id}}" class="button is-danger" onclick="return confirm('Definitieve lessen kunnen niet meer worden gewijzigd.\nWeet je zeker dat je door wil gaan?');">Maak lessen definitief</a>
<a href="{{route('leerling.show',[$leerling])}}" class="button">Terug</a>


</div>


@endsection
