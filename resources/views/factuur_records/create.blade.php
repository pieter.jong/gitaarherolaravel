@extends('layout')

@section('content')

    <h1 class="title">Verkoop product toevoegen</h1>
    <form id="factuur_record_create" action="/leerling/factuur_record/store/{{$leerling->id}}" method="post">
        {{csrf_field()}}
        <div class="section">
            <h2 class="subtitle">Product gegevens</h2>
    
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="***">Product</label>
                        <div id="***" class="select">
                            <select name="verkoop_product">
                                @foreach ($verkoop_producten as $verkoop_product)
                                    <option value="{{$verkoop_product->id}}" {{ (old('verkoop_product')==$verkoop_product->id ) ? 'selected' : ''}}>{{$verkoop_product->omschrijving}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="field is-half">
                        <label class="label" for="omschrijving">Omschrijving</label>
                        <div class="control">
                            <input id="omschrijving" name="omschrijving" class="input {{$errors->has('omschrijving') ? 'is-danger' : '' }}" type="text"
                                placeholder="omschrijving" value="{{ old('omschrijving') }}"
                            />
                        </div>
                    </div>
    
                    <div class="field is-half">
                        <label class="label" for="qwantiteit">Qwantiteit</label>
                        <div class="control">
                            <input id="qwantiteit" name="qwantiteit" class="input {{$errors->has('qwantiteit') ? 'is-danger' : ''}}" type="text" placeholder="qwantiteit"
                                value="{{old('qwantiteit') }}" />
                        </div>
                    </div>
    
                    <div class="field">
                        <label class="label" for="btw_tarief_id">Btw percentage %</label>
                        <div class="control">
                            <input id="btw_tarief_id" name="btw_tarief_id" class="input {{$errors->has('btw_tarief_id') ? 'is-danger' : ''}}" type="text" placeholder="btw_tarief_id"
                                value="{{old('btw_tarief_id') }}" />
                        </div>
                    </div>
    
                    <div class="field">
                        <label class="label" for="tarief">Tarief</label>
                        <div class="control">
                            <input id="tarief" name="tarief" class="input {{$errors->has('tarief') ? 'is-danger' : ''}}" type="text" placeholder="tarief"
                                value="{{old('tarief') }}" />
                        </div>
                    </div>
    
                    <div class="field">
                        <label class="label" for="les_datum">Datum</label>
                        <div class="control">
                            <input id="les_datum" name="les_datum" class="input {{$errors->has('les_datum') ? 'is-danger' : ''}}" type="text" placeholder="les_datum"
                                value="{{old('les_datum') }}" />
                        </div>
                    </div>
    
                    <div class="field">
                        <label class="label" for="les_tijdstip">Tijdstip</label>
                        <div class="control">
                            <input id="les_tijdstip" name="les_tijdstip" class="input {{$errors->has('les_tijdstip') ? 'is-danger' : ''}}" type="text"
                                placeholder="les_tijdstip" value="{{old('les_tijdstip')}}"
                            />
                        </div>
                    </div>
    
                    <div class="field">
                        <label class="label" for="les_minuten">Minuten</label>
                        <div class="control">
                            <input id="les_minuten" name="les_minuten" class="input {{$errors->has('les_minuten') ? 'is-danger' : ''}}" type="text" placeholder="les_minuten"
                                value="{{old('les_minuten')}}" />
                        </div>
                    </div>
                </div>
                <div class="column">
    
                </div>
            </div>
        </div>
    </form>
    <div class="columns">
        <div class="column">
            <button form="factuur_record_create" class="button is-primary">Opslaan</button>
            <a href="{{route('leerling.show',[$leerling])}}">
                <div class="button">Terug</div>
            </a>
        </div>
    </div>
    @include('errors')

@endsection