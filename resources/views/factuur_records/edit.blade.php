@extends('layout')

@section('content')

    <h1 class="title">Product aanpassen</h1>
<form name="factuur_record_edit" id="factuur_record_edit" action="{{$update_route}}" method="post">
        {{csrf_field()}}
        {{method_field('PATCH')}}
        <div class="section">
            <h2 class="subtitle">Product gegevens</h2>
            
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="***">Product</label>
                        <div id="***" class="select">
                        <select name="***" {{($lock) ? 'disabled' :''}}>
                                <option value="5" {{ (old('***') == 5) ? 'selected' : ''}} >product 1</option>
                                <option value="1" {{ (old('***') == 1) ? 'selected' : ''}} selected >product 2</option>
                                <option value="2" {{ (old('***') == 2) ? 'selected' : ''}} >product 3</option>
                                <option value="3" {{ (old('***') == 3) ? 'selected' : ''}} >product 4</option>
                                <option value="4" {{ (old('***') == 4) ? 'selected' : ''}} >product 5</option>
                            </select>
                        </div>
                    </div>
                    <div class="field is-half">
                        <label class="label" for="omschrijving">Omschrijving</label>
                            <div class="control">
                                <input id="omschrijving" name="omschrijving" class="input {{$errors->has('omschrijving') ? 'is-danger' : '' }}" type="text" placeholder="omschrijving" value="{{old('omschrijving') ? old('omschrijving') : $factuur_record->omschrijving}}"/>
                            </div>
                    </div>

                    <div class="field is-half">
                    <label class="label" for="qwantiteit">Qwantiteit</label>
                    <div class="control">
                    <input id="qwantiteit" name="qwantiteit" class="input {{$errors->has('qwantiteit') ? 'is-danger' : ''}}" type="text" placeholder="qwantiteit" value="{{old('qwantiteit') ? old('qwantiteit') : $factuur_record->qwantiteit}}"/>
                    </div>
                    </div>

                    <div class="field">
                    <label class="label" for="btw_tarief_id">Btw percentage %</label>
                    <div class="control">
                    <input id="btw_tarief_id" name="btw_tarief_id" class="input {{$errors->has('btw_tarief_id') ? 'is-danger' : ''}}" type="text" placeholder="btw_tarief_id" value="{{old('btw_tarief_id') ? old('btw_tarief_id') : $factuur_record->btw_tarief_id}}"/>
                    </div>
                    </div>

                    <div class="field">
                    <label class="label" for="tarief">Tarief</label>
                    <div class="control">
                    <input id="tarief" name="tarief" class="input {{$errors->has('tarief') ? 'is-danger' : ''}}" type="text" placeholder="tarief" value="{{old('tarief') ? old('tarief') : $factuur_record->tarief}}"/>
                    </div>
                    </div>

                    <div class="field">
                    <label class="label" for="les_datum">Datum</label>
                    <div class="control">
                    <input id="les_datum" name="les_datum" class="input {{$errors->has('les_datum') ? 'is-danger' : ''}}" type="text" placeholder="les_datum" value="{{old('les_datum') ? old('les_datum') : $factuur_record->les_datum}}"/>
                    </div>
                    </div>

                    <div class="field">
                    <label class="label" for="les_tijdstip">Tijdstip</label>
                    <div class="control">
                    <input id="les_tijdstip" name="les_tijdstip" class="input {{$errors->has('les_tijdstip') ? 'is-danger' : ''}}" type="text" placeholder="les_tijdstip" value="{{old('les_tijdstip') ? old('les_tijdstip') : $factuur_record->les_tijdstip}}"/>
                    </div>
                    </div>

                    <div class="field">
                    <label class="label" for="les_minuten">Minuten</label>
                    <div class="control">
                    <input id="les_minuten" name="les_minuten" class="input {{$errors->has('les_minuten') ? 'is-danger' : ''}}" type="text" placeholder="les_minuten" value="{{old('les_minuten') ? old('les_minuten') : $factuur_record->les_minuten}}"/>
                    </div>
                    </div>
                </div>
                <div class="column">

                </div>
            </div>
        </div>
    </form>
    <div class="columns">
        <div class="column">
            <button form="factuur_record_edit" class="button is-primary">Opslaan</button>
        <a href="{{$back_route}}">
                <div class="button">Terug</div>
            </a>
        </div>
    </div>

    @include('errors')

@endsection