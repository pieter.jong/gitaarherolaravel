@extends('layout')

@section('content')

    <h1 class="title">Overzicht van verkochte items</h1>
    <div class="box">
        @foreach($factuur_record as $record)
            <div class="level has-background-light">
                <div class="level-left">
                    <a href="{{route('leerling.show',[$leerling])}}">
                        {{ $record->naam }}
                        {{ $record->achternaam }}
                    </a>
                </div>
                <div class="level-right">
                    <a class="button is-primary" href="/leerlingen/{{$record->id}}">Bekijken &nbsp;<i class="far fa-eye"></i></a>
                </div>
            </div>

        @endforeach
    </div>
@endsection