<div class="columns is-mobile">
<div class="column is-5"><strong>Omschrijving</strong></div>
<div class="column is-2"><strong>Datum</strong></div>
<div class="column is-2"><strong>Aantal</strong></div>
<div class="column is-1"><strong>Kosten</strong></div>
</div>
@foreach($invoiceRecords as $record)
    <div class="columns is-mobile {{ ($loop->iteration % 2 == 0) ? 'has-background-light' : 'has-background-grey-lighter' }} ">
        <div class="column is-5">
            @if ($record->canceled)
                <strike><span title="Deze les vervalt vanwege een leerling/docent vakantie. &#013;Op het moment van factureren wordt deze definitief verwijderd." class="has-text-danger"><i class="fas fa-exclamation-triangle"></i></span>Deze les vervalt</strike>
            @elseif ($record->omschrijving)
                {{ $record->omschrijving }}
            @elseif ($record->salesItem->omschrijving)
                {{ $record->salesItem->omschrijving }}
            @endif
        </div>
        <div class="column is-2">{!!($record->canceled) ? '<strike>'.$record->les_datum . '</strike>' : $record->les_datum!!}</div>
        <div class="column is-2">{!!($record->canceled) ? '<strike>'.$record->qwantiteit. '</strike>' : $record->qwantiteit !!}</div>
        <div class="column is-1">{!!($record->canceled) ? '<strike>&euro; '. $record->tarief .'</strike>' : '&euro; '. $record->tarief !!}</div>
        <div class="column is-2 is-primary" style="padding:0;">
            <div class="level-right">
            @if (!$record->canceled)
            <a class="button is-primary" href="{{$factuurRecordEditRoute}}{{$record->id}}" title="Bewerken">Bewerken &nbsp;<i class="fas fa-pencil-alt"></i></a>
                {{-- <a class="button is-primary" href="/factuur_record/{{$record->id}}/edit" title="Bewerken">Bewerken &nbsp;<i class="fas fa-pencil-alt"></i></a> --}}
            <a class="button is-danger" href="{{$vakantieCreateCancelLesson}}{{$record->id}}" title="Les annuleren"><i class="fas fa-ban"></i></a>
            @endif

                {{-- <a class="button is-danger" href="/factuur_record/{{$les->id}}"><i class="fas fa-trash"></i></a> --}}
            </div>
        </div>
    </div>
@endforeach
