@extends('layout')

@section('content')

    <h1 class="title">Instellingen</h1>
    <div class="box">
        <div class="level">
                <h2 class="subtitle">Klant contact</h2>
        </div>
        <div class="tile is-ancestor">
            <div class="tile is-parent is-4">
                <a href="/instellingen/communicatie" class="tile is-child notification is-dark">
                    <p class="subtitle">Communicatie instelingen</p>
                    <p class="">Top tile</p>
                    <progress class="progress is-success" value="0" max="100"></progress>
                </a>
            </div>
            <div class="tile is-parent is-4">
                <a href="/instellingen/vakanties" class="tile is-child notification is-dark">
                    <p class="subtitle">Schoolvakanties beheren</p>
                    <p class="">Bottom tile</p>
                    <progress class="progress is-success" value="0" max="100"></progress>
                </a>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="level">
            <h2 class="subtitle">Verkoop / Inkoop afdeling</h2>
        </div>
        <div class="tile is-ancestor">
            <div class="tile is-parent is-4">
                <a href="/instellingen/verkoop_product" class="tile is-child notification is-dark">
                    <p class="subtitle">Verkoop producten</p>
                    <p class="has-text-centered"><span class="title has-text-success"><i class="fas fa-gift"></i></span></p>
                </a>
            </div>
            <div class="tile is-parent is-4">
                <a href="/instellingen/factuurinstellingen" class="tile is-child notification is-dark">
                    <p class="subtitle">Inkoop producten</p>
                    <p class="has-text-centered"><span class="title has-text-warning"><i class="fas fa-coins"></i></span></p>
                    
                </a>
            </div>
            <div class="tile is-parent is-4">
                <a href="/instellingen/factuurinstellingen" class="tile is-child notification is-dark">
                    <p class="subtitle">Factuur instellingen</p>
                    <p class="">Bottom tile</p>
                    <progress class="progress is-success" value="0" max="100"></progress>
                </a>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="level">
            <h2 class="subtitle">Bedrijfsinstellingen</h2>
        </div>
        <div class="tile is-ancestor">
            <div class="tile is-parent is-4">
                <a href="/instellingen/docentgegevens/" class="tile is-child notification is-dark">
                    <p class="subtitle">Bedrijfsgegevens</p>
                    @if($mijn_gegeven_percentage == 100)
                        <p class="">{{$mijn_gegeven_percentage}}% <span class="has-text-success"><i class="fas fa-check"></i></span> </p>
                        <progress class="progress is-success" value="{{$mijn_gegeven_percentage}}" max="100">{{$mijn_gegeven_percentage}}%</progress>
                    @else
                        <p class="">{{$mijn_gegeven_percentage}}% <span title="Niet alle velden zijn ingevuld. Hierdoor kan het systeem niet alle taken uitvoeren" class="has-text-danger"><i class="fas fa-exclamation-triangle"></i></span></p>
                        <progress class="progress is-info" value="{{$mijn_gegeven_percentage}}" max="100">{{$mijn_gegeven_percentage}}%</progress>
                    @endif
                </a>
            </div>
            <div class="tile is-parent is-4">
                <a href="/instellingen/leswaardes/" class="tile is-child notification is-dark">
                    <p class="subtitle">Les waardes</p>
                    @if($lesgegevens_percentage == 100)
                        <p class="">{{$lesgegevens_percentage}}% <span class="has-text-success"><i class="fas fa-check"></i></span> </p>
                        <progress class="progress is-success" value="{{$lesgegevens_percentage}}" max="100">{{$lesgegevens_percentage}}%</progress>
                    @else
                        <p class="">{{$lesgegevens_percentage}}% <span title="Niet alle velden zijn ingevuld. Hierdoor kan het systeem niet alle taken uitvoeren" class="has-text-danger"><i class="fas fa-exclamation-triangle"></i></span></p>
                        <progress class="progress is-info" value="{{$lesgegevens_percentage}}" max="100">{{$lesgegevens_percentage}}%</progress>
                    @endif
                </a>





            </div>
            <div class="tile is-parent is-4">
                <a href="instellingen/docentvakanties" class="tile is-child notification is-dark">
                    <p class="subtitle">Mijn vakanties </p>
                    <p class="has-text-centered"><span class="title has-text-success"><i class="fab fa-pagelines"></i></span></p>

                </a>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="level">
            <h2 class="subtitle">Belasting zaken</h2>
        </div>
        @include('btwtarieven.tarieven')
    </div>



@endsection