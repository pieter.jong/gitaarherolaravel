<!doctype html>
<html>
    <head>
        <title></title>
    </head>
    {{-- <body style="background-image: url({{ asset('/public/achtergrond/optimised_100.jpg') }})"> --}}
    {{-- <body style="background-image: url('/images/guitaradventure_thumbnail.png')"> --}}
    <body>
    <nav class="navbar" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
            <a class="navbar-item" href="/leerling_overzicht">
                {{-- <img src="/images/guitaradventure.png" width="112" height="58"> --}}
                <figure class="image">
                    <img src="/images/guitaradventure_thumbnail.png">
                </figure>
            </a>

            <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>

        <div id="navbarBasicExample" class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item" href="/leerling_overzicht">Leerlingen</a>
                <a class="navbar-item" href="/factuur_administratie" >Factuur administratie</a>
                <a class="navbar-item" href="/belasting">Belastingzaken</a>
                <a class="navbar-item has-text-danger" href="/contact"><i class="fas fa-exclamation-triangle"></i> &nbsp;Alerts</a>

                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link">
                        Meer
                    </a>

                    <div class="navbar-dropdown">
                        <a class="navbar-item" href="/projects">
                            Projecten
                        </a>
                        <a class="navbar-item" href="/instellingen">
                            Instellingen
                        </a>
                    </div>

                </div>
            </div>

            <div class="navbar-end">
                <div class="navbar-item">
                    <div class="buttons">
                        <a class="button is-info" href="/logout">
                            <strong>Uitloggen</strong>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <div class="container">
        @yield('content')
    </div>
    {{--<link rel="stylesheet" href="/public/css/app.css">--}}
    {{--<link rel="stylesheet" href="/node_modules/bulma-extensions/bulma-calendar/dist/css/bulma-calendar.min.css">--}}
    {{--<script src="/node_modules/bulma-extensions/bulma-calendar/dist/js/bulma-calendar.min.js"></script>--}}

    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
{{--    <link rel="stylesheet" href="{{ asset('/css/bulma-calendar.min.css') }}">--}}
    <script src="{{ asset('/js/bulma-calendar.min.js') }}"></script>
    <div class="footer" style="margin-top: 10px; background:transparent"></div>
    <script>
//        var calendars = bulmaCalendar.getElementById('#start_datum');


        // Initialize all input of date type.
        var calendars = bulmaCalendar.attach('[type="date"]', options);
//        var calendars = bulmaCalendar.attach('.date', options);

        // Loop on each calendar initialized
        for(var i = 0; i < calendars.length; i++) {
            // Add listener to date:selected event
            calendars[i].on('date:selected', date => {
                console.log(date);
        });
        }
    </script>
    <div class="background" style="
    position: fixed;
    top:0; left: 0; width: 100%; height: 100%; opacity: 0.1; z-index: -1;
    ">
        <img style="width:100%;" src="/achtergrond/optimised_100.jpg" alt="guitar_adventure_achtergrond">

    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script>
        jQuery('.alert-messages').click(function(){
            hide_error();
        });
        $(document).keyup(function(e) {
            if (e.keyCode === 27) hide_error();
        });
        jQuery('#error-overlay').click(function(){
            hide_error();
        });
        function hide_error(){
            jQuery('#error-overlay').hide();
        }
    </script>
    <script>
        $(document).on('click','.ajax-button',function(e){
            e.preventDefault();
            ajax(this);
        });

        function ajax(dom){
            var ajax_request = $(dom).attr('href');
            var ajax_container = $(dom).closest('.ajax-container');

                $.ajax({
            url     : ajax_request,
            method  : 'get',

            data    : {
                // login_username  : 'flipper',
                // password        : 'flapper'
            },
            success : function(response){
                $(ajax_container).html(response);
            }
            });
        }
    </script>
    </body>
</html>
