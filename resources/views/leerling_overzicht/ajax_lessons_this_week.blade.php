<div class="level" style="justify-content:left;"><a class="ajax-button" href="/ajax_lessons_prev_week/{{$week_difference}}">Vorige week</a> &nbsp; {{$week_difference}} weken
    &nbsp; <a href="/ajax_lessons_next_week/{{$week_difference}}" class="ajax-button">Volgende week</a></div>
@foreach($lessons_this_week as $lesson)
<div class="columns is-mobile {{ ($loop->iteration % 2 == 0) ? 'has-background-light' : 'has-background-grey-lighter' }} ">
    <div class="column is-one-quarter">{{ $lesson->leerling->naam }} {{ $lesson->leerling->achternaam }}</div>
    <div class="column is-2-tablet is-4-mobile">{{ $lesson->les_datum }}</div>
    <div class="column is-1">{{ $lesson->les_tijdstip }}</div>
    <div class="column is-two-quarter is-primary" style="padding:0;">
        <div class="level-right">
            @if (!$lesson->canceled)
            <a class="button is-primary" href="{{$factuur_record_edit_route}}{{$lesson->id}}" title="Bewerken">Bewerken &nbsp;<i class="fas fa-pencil-alt"></i></a>{{-- <a class="button is-primary" href="/factuur_record/{{$record->id}}/edit" title="Bewerken">Bewerken &nbsp;<i class="fas fa-pencil-alt"></i></a>--}}
            <a class="button is-danger" href="{{$vakantie_create_cancel_lesson_route}}{{$lesson->id}}" title="Les annuleren"><i class="fas fa-ban"></i></a>@endif
        </div>
    </div>
    
</div>
@endforeach