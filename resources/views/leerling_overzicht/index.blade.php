@extends('layout')

@section('content')

    <h1 class="title">Leerlingen overzicht</h1>
    <a class="button is-primary" href="{{route('leerling.create')}}">Leerling aanmaken</a>
    <br/>
    <br/>
    <div class="box">
        <div class="level">
            <h1 class="subtitle">Leerlingen overzicht</h1>
        </div>
        @include('leerlingen.index')
    </div>
    <div class="box">
        <div class="level">
            <h1 class="subtitle">Lessen deze week</h1>
        </div>
        <div class="ajax-container lessen-deze-week">
            @include('leerling_overzicht.ajax_lessons_this_week')
        </div>
        
    </div>
    <div class="box">
        <div class="level">
            <h1 class="subtitle">Afwezig deze week</h1>
        </div>
    </div>
    <div class="box">
        <div class="level">
            <h1 class="subtitle">Docent vakanties</h1>
        </div>
    </div>

@endsection
