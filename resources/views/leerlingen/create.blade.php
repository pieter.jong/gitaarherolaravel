@extends('layout')

@section('content')

    <h1 class="title">Leerling aanmaken</h1>
    <form name="leerlingen_create" id="leerlingen_create" action="{{route('leerling.store')}}" method="post">
        {{csrf_field()}}
        <div class="section">
            <h2 class="subtitle">Leerling gegevens</h2>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="title">Naam</label>
                        <div class="control">
                            <input id="title" name="naam" class="input {{$errors->has('naam') ? 'is-danger' : ''}}" type="text" placeholder="Naam" value="{{ old('naam') }}"/>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="achternaam">Achternaam</label>
                        <div class="control">
                            <input id="achternaam" name="achternaam" class="input {{$errors->has('achternaam') ? 'is-danger' : ''}}" type="text" placeholder="achternaam" value="{{ old('achternaam') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="geadreseerde">Geadreseerde</label>

                        <div class="control">
                            <input id="geadreseerde" name="geadreseerde" class="input" type="text" placeholder="Geadreseerde" value="{{ old('geadreseerde') }}"/>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="email">E-mail</label>
                        <div class="control">
                            <input id="email" name="email" class="input" type="text" placeholder="E-mail" value="{{ old('email') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-half">
                    <div class="field">
                        <label class="label" for="geboortedatum">geboortedatum</label>

                        <div class="control">
                            <input id="geboortedatum" name="geboortedatum" class="input" type="text" placeholder="geboortedatum"
                                   value="{{ old('geboortedatum') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <h2 class="subtitle">Adres gegevens</h2>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="straatnaam">straatnaam</label>

                        <div class="control">
                            <input id="straatnaam" name="straatnaam" class="input" type="text" placeholder="straatnaam"
                                   value="{{ old('straatnaam') }}"/>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="huisnummer">huisnummer</label>

                        <div class="control">
                            <input id="huisnummer" name="huisnummer" class="input" type="text" placeholder="huisnummer"
                                   value="{{ old('huisnummer') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="postcode">postcode</label>

                        <div class="control">
                            <input id="postcode" name="postcode" class="input" type="text" placeholder="postcode"
                                   value="{{ old('postcode') }}"/>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="woonplaats">woonplaats</label>

                        <div class="control">
                            <input id="woonplaats" name="woonplaats" class="input" type="text" placeholder="woonplaats"
                                   value="{{ old('woonplaats') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-half">
                    <div class="field">
                        <label class="label" for="telefoon">telefoon</label>

                        <div class="control">
                            <input id="telefoon" name="telefoon" class="input" type="text" placeholder="telefoon"
                                   value="{{ old('telefoon') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <h2 class="subtitle">Les gegevens</h2>
            <div class="column">
                <label class="checkbox">
                    <input value="1" name="geen_les_meer" type="checkbox" >
                    Heeft geen les meer
                </label>
            </div>
            <div class="columns">
                <div class="column is-half">
                    <div class="field">
                        <label class="label" for="eerste_les">eerste_les</label>

                        <div class="control">
                            <input id="eerste_les" name="eerste_les" class="input" type="text" placeholder="dd-mm-yyyy" value="{{ old('eerste_les') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="les_interval">les_interval</label>
                        <div id="les_interval" class="select">
                            <select name="les_interval">
                                <option value="1" {{ (old('les_interval') == 1) ? 'selected' : ''}}>1x per week</option>
                                <option value="2" {{ (old('les_interval') == 2) ? 'selected' : ''}}>1x per 2 week</option>
                                <option value="3" {{ (old('les_interval') == 3) ? 'selected' : ''}}>1x per 3 week</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="les_type">les_type</label>
                        <div id="les_type" class="select">
                            <select name="les_type">
                                <option value="1" >Prive</option>
                                <option value="2" >Groepsles</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="column">
                    <div class="field">
                        <label class="label" for="les_dag">les_dag</label>
                        <div id="les_dag" class="select">
                            <select name="les_dag">
                                <option value="1" {{ (old('les_dag') == 1) ? 'selected' : ''}}>maandag</option>
                                <option value="2" {{ (old('les_dag') == 2) ? 'selected' : ''}}>dinsdag</option>
                                <option value="3" {{ (old('les_dag') == 3) ? 'selected' : ''}}>woensdag!!</option>
                                <option value="4" {{ (old('les_dag') == 4) ? 'selected' : ''}}>donderdag</option>
                                <option value="5" {{ (old('les_dag') == 5) ? 'selected' : ''}}>vrijdag</option>
                                <option value="6" {{ (old('les_dag') == 6) ? 'selected' : ''}}>zaterdag</option>
                                <option value="7" {{ (old('les_dag') == 7) ? 'selected' : ''}}>zondag</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="les_tijdstip">les_tijdstip</label>

                        <div class="control">
                            <input id="les_tijdstip" name="les_tijdstip" class="input" type="text" placeholder="00:00"
                                   value="{{ old('les_tijdstip') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="les_minuten">les_minuten</label>

                        <div class="control">
                            <input id="les_minuten" name="les_minuten" class="input" type="text" placeholder="les_minuten"
                                   value="{{ old('les_minuten') }}"/>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="les_tarief">les_tarief</label>

                        <div class="control">
                            <input id="les_tarief" name="les_tarief" class="input" type="text" placeholder="les_tarief"
                                   value="{{ old('les_tarief') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <h2 class="subtitle">Factuur instellingen</h2>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="***">Betaling methode</label>
                        {{-- <div id="***" class="select">
                            <select name="***">
                                <option value="1" {{ (old('***') == 1) ? 'selected' : ''}} >vast bedrag per maand</option>
                                <option value="2" {{ (old('***') == 2) ? 'selected' : ''}} >1 maand vooruit rekenen</option>
                                <option value="3" {{ (old('***') == 3) ? 'selected' : ''}} >maandelijks achteraf berekenen</option>
                                <option value="4" {{ (old('***') == 4) ? 'selected' : ''}} >factuur per jaar</option>
                                <option value="5" {{ (old('***') == 5) ? 'selected' : ''}} >factuur per halfjaar</option>
                            </select>
                        </div> --}}
                    </div>
                </div>
                <div class="column">

                </div>
            </div>
        </div>
    </form>
    <div class="columns">
        <div class="column">
            <button form="leerlingen_create" class="button is-primary">Opslaan</button>
            <a href="/leerling_overzicht">
                <div class="button">Terug</div>
            </a>
        </div>
    </div>

    @include('errors')

@endsection