@foreach($leerlingen as $leerling)
    {{-- <div class="level has-background-light">
        <div class="level-left">
            <a href="/leerlingen/{{$leerling->id}}">
                {{ $leerling->naam }}
                {{ $leerling->achternaam }}
            </a>
        </div>
        <div class="level-right">
            <a class="button is-primary" href="/leerlingen/{{$leerling->id}}">Bekijken &nbsp;<i class="far fa-eye"></i></a>
        </div>
    </div> --}}


<div class="columns is-mobile {{ ($loop->iteration % 2 == 0) ? 'has-background-light' : 'has-background-grey-lighter' }} ">
    <div class="column is-one-quarter">{{ $leerling->naam }} {{ $leerling->achternaam }}</div>
    <div class="column is-two-quarter is-primary" style="padding:0;">
        <div class="level-right">
            <a class="button is-primary" href="{{ route('leerling.show', [$leerling]) }}">Bekijken &nbsp;<i class="far fa-eye"></i></a>
        </div>
    </div>

</div>


@endforeach