@extends('layout')

@section('content')
    <div class="level">
        <div class="level-item"><a class="button has-background-grey-light is-fullwidth" href="/leerling_overzicht">Terug</a></div>
        <div class="level-item"></div>
        <div class="level-item"></div>
        <div class="level-item"></div>
    </div>
    <div class="box">
        <div class="level">
            <div class="is-grouped">
                <h1 class="title is-inline">
                    @if($pupil->geen_les_meer)
                        <p class="has-text-danger">Heeft geen les meer!</p>
                        <span style="text-decoration: line-through">{{ $pupil->naam }} {{ $pupil->achternaam }}</span>
                    @else
                        {{ $pupil->naam }} {{ $pupil->achternaam }}
                    @endif
                </h1>
                <div class="control is-inline">
                    <a href="{{route('leerling.edit',[$pupil])}}" class="button has-background-grey-light">Bewerken&nbsp;<i class="fas fa-edit"></i></a>
                </div>
                <form class="control is-inline" action="{{route('leerling.destroy', [$pupil])}}" method="post">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="button is-danger" type="submit" onclick="return confirm(' Weet je zeker dat je deze leerling wil verwijderen?');">Verwijderen &nbsp;<i class="fas fa-trash"></i></button>
                </form>
            </div>
        </div>


        <div class="level">
            <div class="level-item level-left">
                <table class="table">
                    <tbody>
                    <tr>
                        <td>Naam</td>
                        <td>{{$pupil->naam}} {{$pupil->achternaam}}</td>

                    </tr>
                    <tr>
                        <td>geadreseerde</td>
                        <td>{{$pupil->geadreseerde}}</td>
                    </tr>
                    <tr>
                        <td>email</td>
                        <td>{{$pupil->email}}</td>
                    </tr>
                    <tr>
                        <td>geboortedatum</td>
                        <td>{{$pupil->geboortedatum}}</td>
                    </tr>
                    <tr>
                        <td>straatnaam</td>
                        <td>{{$pupil->straatnaam}} {{$pupil->huisnummer}}</td>
                    </tr>
                    <tr>
                        <td>postcode</td>
                        <td>{{$pupil->postcode}}</td>
                    </tr>
                    <tr>
                        <td>woonplaats</td>
                        <td>{{$pupil->woonplaats}}</td>
                    </tr>
                    <tr>
                        <td>Telefoon</td>
                        <td>{{$pupil->telefoon}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="level-item level-left">
                <table class="table">
                    <tbody>
                        {{-- <tr>
                            <td>Activatie datum van onderstaande instellingen</td>
                            <td>{{$pupil->eerste_les}}</td>
                        </tr> --}}
                    <tr>
                        <td><span title="Let Op! Deze wijziging gaat pas in vanaf dd-mm-YY" class="has-text-danger"><i class="fas fa-exclamation-triangle"></i></span> les_interval</td>
                        <td>{{$pupil->les_interval}}</td>
                    </tr>
                    <tr>
                        <td><span title="Let Op! Deze wijziging gaat pas in vanaf dd-mm-YY" class="has-text-danger"><i class="fas fa-exclamation-triangle"></i></span> les_type</td>
                        <td>{{$pupil->les_type}}</td>
                    </tr>
                    <tr>
                        <td><span title="Let Op! Deze wijziging gaat pas in vanaf dd-mm-YY" class="has-text-danger"><i class="fas fa-exclamation-triangle"></i></span> les_dag</td>
                        <td>{{$pupil->les_dag}}</td>
                    </tr>
                    <tr>
                        <td><span title="Let Op! Deze wijziging gaat pas in vanaf dd-mm-YY" class="has-text-danger"><i class="fas fa-exclamation-triangle"></i></span> Tijdstip</td>
                        <td>{{$pupil->les_tijdstip}}</td>
                    </tr>
                    <tr>
                        <td><span title="Let Op! Deze wijziging gaat pas in vanaf dd-mm-YY" class="has-text-danger"><i class="fas fa-exclamation-triangle"></i></span> Aantal minuten les</td>
                        <td>{{$pupil->les_minuten}}</td>
                    </tr>
                    <tr>
                        <td><span title="Let Op! Deze wijziging gaat pas in vanaf dd-mm-YY" class="has-text-danger"><i class="fas fa-exclamation-triangle"></i></span> Tarief</td>
                        <td>{{$pupil->les_tarief}}</td>
                    </tr>
                    <tr>
                        <td>Eerste les</td>
                        <td>{{$pupil->eerste_les}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>



    </div>

    <div class="box">
        <div class="level">
            <div class="is-grouped">
                <h1 class="subtitle is-inline">Vakanties en uitzonderingen</h1>
                <div class="control is-inline">
                    <a href="/leerlingen/{{$pupil->id}}/leerlingvakantie" class="button has-background-grey-light">Toevoegen &nbsp;<i class="far fa-plus-square"></i></a>
                </div>
            </div>

        </div>
        @include('vakanties.leerlingVakanties')
    </div>
    <div class="box">
        <div class="level">
            <div class="is-grouped">
                <h1 class="subtitle is-inline">Docent vakanties</h1>
                <div class="control is-inline">
                    <a href="/leerlingen/{{$pupil->id}}/docentvakantie" class="button has-background-grey-light">Toevoegen &nbsp;<i class="far fa-plus-square"></i></a>
                </div>
            </div>

        </div>
        @include('vakanties.docentVakanties')
    </div>

    <div class="box">
        <div class="level"><h1 class="subtitle">Facturen</h1></div>
        <div class="level"><a class="button is-info" href="/geplande_factuur/{{$pupil->id}}">Bekijk komende factuur &nbsp;<i class="far fa-eye"></i></a></div>
        <p>Factuur records</p>
    </div>
    <div class="box">
        <div class="is-grouped is-inline">
            <h1 class="subtitle is-inline">Geplande lessen / producten</h1>
        </div>
        <div class="control is-inline">
            <a href="/leerling/factuur_record/get/{{$pupil->id}}" class="button has-background-grey-light">Toevoegen &nbsp;<i class="far fa-plus-square"></i></a>
        </div>
        @include('factuur_records.lessen')
    </div>
    <div class="box">
        <div class="is-grouped">
            <h1 class="subtitle is-inline">Verkochte lessen / producten</h1>
        </div>
    </div>

    <div class="level">
        <div class="level-item"><a class="button has-background-grey-light is-fullwidth" href="/leerling_overzicht">Terug</a></div>
        <div class="level-item"></div>
        <div class="level-item"></div>
        <div class="level-item"></div>
    </div>
    @include('errors')
    @include('flash_messages')


@endsection
