<!doctype html>
<html>
    <head>
        <title></title>
    </head>
    {{-- <body style="background-image: url({{ asset('/public/achtergrond/optimised_100.jpg') }})"> --}}
    {{-- <body style="background-image: url('/images/guitaradventure_thumbnail.png')"> --}}
    <body>
    <div class="container">
        @yield('content')
    </div>
    {{--<link rel="stylesheet" href="/public/css/app.css">--}}
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/bulma-calendar.min.css') }}">
    {{--<link rel="stylesheet" href="/node_modules/bulma-extensions/bulma-calendar/dist/css/bulma-calendar.min.css">--}}
    {{--<script src="/node_modules/bulma-extensions/bulma-calendar/dist/js/bulma-calendar.min.js"></script>--}}
    <script src="{{ asset('/js/bulma-calendar.min.js') }}"></script>
    <div class="footer has-background-black"></div>
    <script>
//        var calendars = bulmaCalendar.getElementById('#start_datum');


        // Initialize all input of date type.
        var calendars = bulmaCalendar.attach('[type="date"]', options);
//        var calendars = bulmaCalendar.attach('.date', options);

        // Loop on each calendar initialized
        for(var i = 0; i < calendars.length; i++) {
            // Add listener to date:selected event
            calendars[i].on('date:selected', date => {
                console.log(date);
        });
        }
    </script>
    <div class="background" style="
    position: fixed; 
    top: 0; left: 0; width: 100%; height: 100%; opacity: 0.1; z-index: -1;
    ">
        <img style="width:100%;" src="/achtergrond/optimised_100.jpg" alt="guitar_adventure_achtergrond">
        
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script>
        jQuery(window).on('.alert-messages','click',function(){
            alert('ik werk');
        });
        jQuery('.alert-messages').click(function(){
            jQuery('#error-overlay').hide();
        });
    </script>
    </body>
</html>
