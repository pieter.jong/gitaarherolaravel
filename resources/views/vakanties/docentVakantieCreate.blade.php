@extends('layout')

@section('content')

    <h1 class="title">Vakantie voor alle leerlingen</h1>
        @include('vakanties.docent_vakantie_create_form')
    <div class="columns">
        <div class="column">
            <button form="leerlingvakantie_create"  class="button is-primary">Opslaan</button>
            <a href="{{route('leerling.show',[$id])}}">
                <div class="button">Terug</div>
            </a>
        </div>
    </div>

    @include('errors')

@endsection