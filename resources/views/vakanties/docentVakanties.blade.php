@if($teacherAbsence)
    @foreach($teacherAbsence as $vakantie)
        <div class="level has-background-light">
            <div class="level-item level-left">
                <div class="level-item">{{$vakantie->start_datum}}</div>
                <div class="level-item">{{$vakantie->eind_datum}}</div>
                <div class="level-item">{{$vakantie->omschrijving}}</div>
            </div>
            <div class="level-right">
                <a class="button is-primary" href="/leerlingen/{{$pupil->id}}/docentvakantie/{{$vakantie->id}}">Bewerken &nbsp;<i class="fas fa-edit"></i></a>
                <form action="/leerlingen/{{$pupil->id}}/docentvakantie/{{$vakantie->id}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit"  class="button is-danger" onclick="return confirm(' Weet je zeker dat je deze vakantie wil verwijderen?');"><i class="fas fa-trash"></i></button>
                </form>
            </div>
        </div>
    @endforeach
@endif

