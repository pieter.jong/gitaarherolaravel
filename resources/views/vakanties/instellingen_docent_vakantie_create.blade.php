@extends('layout')

@section('content')

    <h1 class="title">Dit heeft een lagere prioriteit!!</h1>
    {{--<h1 class="title">Vakantie voor alle leerlingen</h1>--}}
        {{--@include('vakanties.docent_vakantie_create_form')--}}
    <div class="columns">
        <div class="column">
            <button form="leerlingvakantie_create" formaction="" class="button is-primary">Opslaan</button>
            <a href="/instellingen">
                <div class="button">Terug</div>
            </a>
        </div>
    </div>

    {{--@include('errors')--}}

@endsection