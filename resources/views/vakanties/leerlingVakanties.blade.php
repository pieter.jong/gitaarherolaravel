@if($pupil->vakanties->count())
<div class="columns is-mobile">
    <div class="column is-2"><strong>Startdatum</strong></div>
    <div class="column is-2"><strong>Einddatum</strong></div>
    <div class="column is-5 "><strong>Omschrijving</strong></div>
</div>
@foreach($pupil->vakanties as $vakantie)
<div class="columns is-mobile {{ ($loop->iteration % 2 == 0) ? 'has-background-light' : 'has-background-grey-lighter' }} ">
    <div class="column is-2">{{$vakantie->start_datum}} </div>
    <div class="column is-2">{{$vakantie->eind_datum}}</div>
    <div class="column is-5">{{$vakantie->omschrijving}}</div>
    <div class="column is-3 is-primary" style="padding:0;">
        <div class="level-right">
            <a class="button is-primary" href="/leerlingen/vakantie/{{$vakantie->id}}">Bewerken &nbsp;<i class="fas fa-edit"></i></a>
            <form action="/leerlingen/vakantie/{{$vakantie->id}}" method="POST">
                @method('DELETE') @csrf
                <button type="submit" class="button is-danger" onclick="return confirm(' Weet je zeker dat je deze vakantie wil verwijderen?');"><i class="fas fa-trash"></i></button>
            </form>
        </div>
    </div>
</div>
@endforeach @endif
