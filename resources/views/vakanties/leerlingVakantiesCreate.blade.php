@extends('layout')

@section('content')

    <h1 class="title">{{$leerling->naam}} {{$leerling->achternaam}}</h1>
    <form name="leerlingvakantie_create" id="leerlingvakantie_create" action="/leerlingen/{{$leerling->id}}/leerlingvakantie" method="post">
        {{csrf_field()}}
        <div class="section">
            <h2 class="subtitle">Vakantie toevoegen</h2>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="start_datum">start_datum</label>

                        <div class="control">
                            <input id="start_datum" name="start_datum" class="input {{$errors->has('start_datum') ? 'is-danger' : ''}}" type="text" placeholder="start_datum" value="{{ old('start_datum') }}"/>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="eind_datum">eind_datum</label>

                        <div class="control">
                            <input id="eind_datum" name="eind_datum" class="input {{$errors->has('eind_datum') ? 'is-danger' : ''}}" type="text" placeholder="eind_datum" value="{{ old('eind_datum') }}"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field">
                <label class="label" for="omschrijving">omschrijving</label>

                <div class="control">
                    <input id="omschrijving" name="omschrijving" class="input {{$errors->has('omschrijving') ? 'is-danger' : ''}}" type="text"
                           placeholder="omschrijving" value="{{ old('omschrijving') }}"/>
                </div>
            </div>
        </div>
    </form>
    <div class="columns">
        <div class="column">
            <button form="leerlingvakantie_create" class="button is-primary">Opslaan</button>
            <a href="{{route('leerling.show', $leerling)}}">
                <div class="button">Terug</div>
            </a>
        </div>
    </div>

    @include('errors')

@endsection