@extends('layout')

@section('content')

    <h1 class="title">Verkoop product toevoegen</h1>
    
    <div class="box">
        <form name="product_create" id="product_create" action="/instellingen/verkoop_product/" method="post">
            {{csrf_field()}}
            <h2 class="subtitle">Product gegevens</h2>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="omschrijving">Omschrijving</label>
                        <div class="control">
                            <input id="omschrijving" name="omschrijving" class="input {{$errors->has('omschrijving') ? 'is-danger' : ''}}" type="text"
                                placeholder="omschrijving" value="{{ old('omschrijving') }}" />
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label class="label" for="tarief">Tarief</label>
                        <div class="control">
                            <input id="tarief" name="tarief" class="input {{$errors->has('tarief') ? 'is-danger' : ''}}" type="text" placeholder="tarief"
                                value="{{ old('tarief') }}" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label class="label" for="btw_tarief_id">Btw tarief</label>
                        <div id="btw_tarief_id" class="select">
                            <select name="btw_tarief_id">
                                @foreach ($btw_tarieven as $item)
                            <option value="{{$item->id}}" {{ (old('btw_tarief_id') == 1) ? 'selected' : ''}}>{{$item->omschrijving}}</option>
                                    
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="column">
            
                </div>
            </div>
        </form>
        <div class="columns">
            <div class="column">
                <button form="product_create" class="button is-primary">Opslaan</button>
                <a href="/instellingen/">
                    <div class="button">Terug</div>
                </a>
            </div>
        </div>
    </div>
    

    @include('errors')

@endsection