@extends('layout') 
@section('content')

<h1 class="title">Verkoop product bewerken</h1>

<div class="box">
<form name="product_create" id="product_create" action="/instellingen/verkoop_product/{{$verkoop_product->id}}" method="post">
        {{csrf_field()}}
        {{method_field('PATCH')}}
        <h2 class="subtitle">Product gegevens</h2>
        <div class="columns">
            <div class="column">
                <div class="field">
                <label class="label" for="omschrijving">omschrijving</label>
                <div class="control">
                <input id="omschrijving" name="omschrijving" class="input {{$errors->has('omschrijving') ? 'is-danger' : ''}}" type="text" placeholder="omschrijving" value="{{old('omschrijving') ? old('omschrijving') : $verkoop_product->omschrijving}}"/>
                </div>
                </div>
            </div>
            <div class="column">
                <div class="field">
                <label class="label" for="tarief">tarief</label>
                <div class="control">
                <input id="tarief" name="tarief" class="input {{$errors->has('tarief') ? 'is-danger' : ''}}" type="text" placeholder="tarief" value="{{old('tarief') ? old('tarief') : $verkoop_product->tarief}}"/>
                </div>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="column">
                <div class="field">
                    <label class="label" for="btw_tarief_id">Btw tarief</label>
                    <div id="btw_tarief_id" class="select">
                        <select name="btw_tarief_id">
                            @foreach ($btw_tarieven as $item)
                                <option value="{{$item->id}}" {{ old( 'btw_tarief_id') ? (old( 'btw_tarief_id')==$item->id ) ? 'selected' : '' :$verkoop_product->btw_tarief_id == $item->id ? 'selected' : ''}}>{{$item->omschrijving}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="column">

            </div>
        </div>
    </form>
    <div class="columns">
        <div class="column">
            <button form="product_create" class="button is-primary">Opslaan</button>
            <a href="/instellingen/verkoop_product">
                <div class="button">Terug</div>
            </a>
        </div>
    </div>
</div>
    @include('errors')
@endsection