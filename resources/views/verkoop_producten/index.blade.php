@extends('layout')

@section('content')

    <h1 class="title">Verkoop poroducten</h1>
    <div class="box">
        <div class="level">
            <div class="is-grouped">
                <h1 class="subtitle is-inline">Producten</h1>
                <div class="control is-inline">
                    <a href="/instellingen/verkoop_product/create" class="button has-background-grey-light">Toevoegen &nbsp;<i class="far fa-plus-square"></i></a>
                </div>
            </div>
        </div>
        <div class="columns is-mobile">
            <div class="column is-5 "><strong>Omschrijving</strong></div>
            <div class="column is-2"><strong>Btw percentage</strong></div>
            <div class="column is-2"><strong>Btw omschrijving</strong></div>
        </div>
        @foreach($verkoop_producten as $record)
        <div class="columns is-mobile {{ ($loop->iteration % 2 == 0) ? 'has-background-light' : 'has-background-grey-lighter' }} ">
            <div class="column is-5">{{ $record->omschrijving }} </div>
            <div class="column is-2">&euro; {{ $record->tarief }}</div>
            <div class="column is-2">{{ $record->btw_tarief->omschrijving }}</div>
            <div class="column is-3 is-primary" style="padding:0;">
                <div class="level-right">
                    <a class="button is-primary" href="/instellingen/verkoop_product/{{$record->id}}/edit">Bewerken &nbsp;<i class="fas fa-pencil-alt"></i></a>
                    @if(!$record->default)
                    <form class="control is-inline" action="/instellingen/verkoop_product/{{$record->id}}" method="post">
                        {{csrf_field()}} {{method_field('DELETE')}}
                        <button class="button is-danger" type="submit" onclick="return confirm(' Weet je zeker dat je dit product wil verwijderen?');"><i class="fas fa-trash"></i></button>
                    </form>
                    @endif
                </div>
            </div>
        </div>
        @endforeach
        <a class="button" href="/instellingen">Terug</a>
    </div>

    




@endsection



