<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('ajax_lessons_prev_week/{week_difference}', 'LeerlingOverzichtController@ajax_lessons_prev_week');
Route::get('ajax_lessons_next_week/{week_difference}', 'LeerlingOverzichtController@ajax_lessons_next_week');

Route::resource('leerling','LeerlingController'); // maakt achter de schermen alle onderstaande listeners aan.
//Route::get('/test', 'InstellingenConroller@index')->name('showTest'); // route name die je kan gebruiken in de rest van je code.



Route::get('/instellingen', 'InstellingenController@index');
Route::get('/instellingen/verkoop_producten', 'InstellingenController@verkoop_producten_form'); // wordt nog niet gebruikt
Route::post('/instellingen/verkoop_producten', 'InstellingenController@verkoop_producten_write'); // wordt nog niet gebruikt
//Route::get('instellingen/leswaardes', 'DocentInstellingenController@index');
Route::get('instellingen/docentgegevens/', 'DocentInstellingenController@docent_gegevens_edit');
Route::patch('instellingen/docentgegevens/', 'DocentInstellingenController@docent_gegevens_update');
Route::get('instellingen/leswaardes/', 'DocentInstellingenController@leswaardes_edit');
Route::patch('instellingen/leswaardes/', 'DocentInstellingenController@leswaardes_update');
// Route::get('instellingen/docentvakanties/', 'DocentVakantieController@instellingen_create');



Route::get('/factuur_administratie','FactuurAdministratieController@index');
Route::post('/factuur_administratie/genererate_lessons','FactuurAdministratieController@generateLessons');
Route::get('/generate_invoice', 'FactuurAdministratieController@generateInvoice');
Route::get('/geplande_factuur/{leerling_id}', 'FactuurAdministratieController@scheduledInvoice');


Route::get('leerlingen/{leerlingen}/leerlingvakantie', 'VakantieController@create'); // nieuw dus geen id nodig.
Route::get('leerlingen/cancel_lesson/{record_id}', 'VakantieController@create_cancel_lesson'); // nieuw dus geen id nodig.
Route::post('leerlingen/{leerling}/leerlingvakantie', 'VakantieController@store');
Route::get('leerlingen/vakantie/{vakantie}', 'VakantieController@edit');
Route::patch('leerlingen/vakantie/{vakantie}', 'VakantieController@update');
Route::delete('leerlingen/vakantie/{vakantie}', 'VakantieController@destroy');


Route::get('leerlingen/{leerlingen}/docentvakantie', 'DocentVakantieController@create');
Route::post('leerlingen/{leerlingen}/docentvakantie', 'DocentVakantieController@store');
Route::get('leerlingen/{leerlingen}/docentvakantie/{docentvakantie}', 'DocentVakantieController@edit'); // nieuw dus geen id nodig.
Route::patch('leerlingen/{leerlingen}/docentvakantie/{docentvakantie}', 'DocentVakantieController@update');
Route::delete('leerlingen/{leerling}/docentvakantie/{docentvakantie}', 'DocentVakantieController@destroy');

Route::get('/belasting', 'BelastingController@index');


Route::get('btwtarief/create', 'BtwTarievenController@create');
Route::post('btwtarief', 'BtwTarievenController@store');
Route::get('btwtarief/{btwtarief}/edit', 'BtwTarievenController@edit');
Route::patch('btwtarief/{btwtarief}', 'BtwTarievenController@update');
Route::delete('btwtarief/{btwtarief}', 'BtwTarievenController@destroy');

Route::get('factuur_record/create', 'FactuurRecordController@create');
Route::post('factuur_record', 'FactuurRecordController@store');
Route::get('factuur_record/{record}/edit', 'FactuurRecordController@edit');
Route::patch('factuur_record/{record}', 'FactuurRecordController@update');
Route::delete('factuur_record/{record}', 'FactuurRecordController@destroy');

Route::resource('instellingen/verkoop_product', 'VerkoopProductController');

Route::get('leerling/factuur_record/get/{leerling_id}', 'FactuurRecordController@create_for_leerling');
Route::post('leerling/factuur_record/store/{leerling_id}', 'FactuurRecordController@store_for_leerling');
Route::get('leerling/factuur_record/edit/{factuur_record_id}', 'FactuurRecordController@edit_for_leerling');
Route::patch('leerling/factuur_record/update/{factuur_record_id}', 'FactuurRecordController@update_for_leerling');
Route::delete('leerling/factuur_record/delete/{factuur_record_id}', 'FactuurRecordController@destroy_for_leerling');


Route::get('leerling_overzicht', 'LeerlingOverzichtController@index')->name('leerlingoverzicht');
Route::delete('leerling_overzicht/factuur_record/edit/{factuur_record_id}', 'FactuurRecordController@edit_for_leerlingoverzicht');
Route::get('leerling_overzicht/vakantie/create_cancel_lesson/{factuur_record_id}', 'VakantieController@create_cancel_lesson_for_leerling_overzicht');

// create form (Is deze situatie altijd hetzelfde)
// We gaan uit van het idee dat de template zo goed als hetzelfde is.




//Route::get('/leerlingen', 'LeerlingenController@index');
//Route::get('/leerlingen/create', 'LeerlingenController@create');
//Route::post('/leerlingen/', 'LeerlingenController@store');
//Route::get('/leerlingen/{project}', 'LeerlingenController@show');
//Route::get('/leerlingen/{project}/edit', 'LeerlingenController@edit');
//Route::patch('/leerlingen/{project}', 'LeerlingenController@update');
//Route::delete('/leerlingen/{project}', 'LeerlingenController@destroy');


//Route::post('/projects/{project}/tasks','ProjectTasksController@store');
//Route::patch('/tasks/{task}','ProjectTasksController@update');







